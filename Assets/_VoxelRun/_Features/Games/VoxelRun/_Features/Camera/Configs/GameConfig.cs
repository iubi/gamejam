﻿using UnityEngine;

public partial class GameConfig : ScriptableObject
{
    [Header("Camera")]
    public float CameraMovementSpeed;
    public float CameraRotationSpeed;
    public float CameraRootMultiplierOnScale;
    public float CameraRootMultiplierInVoxelState;
    public float CameraTransitionTimeInVoxelState;
    public float CameraTransitionTimeInSolidState;
}
