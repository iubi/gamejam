﻿//using MSKit.Configurations;
using UnityEngine;
using DG.Tweening;

public class VRCameraController : MonoBehaviour
{
    [SerializeField] private GameConfig _configs;
    [SerializeField] private Transform _cameraMovingObject;
    [SerializeField] private Transform _cameraRoot;
    [SerializeField] private Transform _cameraInitialPosition;
    [SerializeField] private Transform _cameraShakeTransform;
    //[SerializeField] private ParticleSystem _airParticles;

    private bool _stopMovement;
    private EvalPoint _targetPoint;
    private VRPlayerState _playerState;

    private Sequence _cameraRootSeq;

    private void Awake()
    {
        Initialize();
    }

    private void Start()
    {
        LoadGame();
        StartGame();
    }

    public void Initialize()
    {
        StopMovement();
        RegisterEvents();
    }

    private void RegisterEvents()
    {
        EventManager.OnPlayerPointUpdate += UpdateTargetPoint;
        EventManager.OnPlayerScaleUpdate += UpdateCameraRoot;
        EventManager.OnPlayerStateChange += CameraStateChange;
        EventManager.OnShakeCamera += ShakeCamera;
    }

    public void LoadGame()
    {
        ResetCamera();
        ForceMove();

        ResetShake();

        _playerState = VRPlayerState.Solid;
    }

    public void StartGame()
    {
        StartMovement();
    }

    public void CompleteGame()
    {
        StopMovement();
        CameraStateChange(VRPlayerState.Solid);
    }

    public void FailGame()
    {
        StopMovement();
        CameraStateChange(VRPlayerState.Solid);
    }

    public void FixedUpdate()
    {
        if (_stopMovement) return;

        MoveCamera();
    }

    private void MoveCamera()
    {
        var movementSpeed = _configs.CameraMovementSpeed;
        var rotationSpeed = _configs.CameraRotationSpeed;
        var targetPosition = _targetPoint.Point;
        var nextPosition = Vector3.Lerp(_cameraMovingObject.position, targetPosition, movementSpeed * Time.deltaTime);

        _cameraMovingObject.position = nextPosition;
        if(_targetPoint.Tangent != Vector3.zero)
        {
            var rotation = Quaternion.LookRotation(_targetPoint.Tangent);
            var nextRotation = Quaternion.Lerp(_cameraMovingObject.rotation, rotation, rotationSpeed * Time.deltaTime);
            _cameraMovingObject.rotation = nextRotation;
        }

    }

    private void ForceMove()
    {
        _cameraMovingObject.position = _targetPoint.Point;
        _cameraMovingObject.rotation = Quaternion.LookRotation(_targetPoint.Tangent);
    }

    private void ResetCamera()
    {
        KillCameraRootSeq();
        _cameraRoot.localPosition = _cameraInitialPosition.localPosition;
        _cameraRoot.localRotation = _cameraInitialPosition.localRotation;
    }

    private void UpdateTargetPoint(EvalPoint point) => _targetPoint = point;

    private void UpdateCameraRoot(float size)
    {
        bool isSolidState = _playerState == VRPlayerState.Solid;
        var position = _cameraInitialPosition.localPosition;

        if (isSolidState)
        {
            size *= _configs.CameraRootMultiplierOnScale;
            if (size < 1) size = 1;
            position *= size;
        }
        else
        {
            size *= _configs.CameraRootMultiplierInVoxelState;
            position += (position * size);
        }

        var time = isSolidState ? _configs.CameraTransitionTimeInSolidState
            : _configs.CameraTransitionTimeInVoxelState;


        KillCameraRootSeq();
        _cameraRootSeq = DOTween.Sequence()
            .Append(_cameraRoot.DOLocalMove(position, time).SetEase(Ease.InOutQuad));
    }

    private void CameraStateChange(VRPlayerState state)
    {
        _playerState = state;

        //if (_playerState == VRPlayerState.Solid)
        //    _airParticles.Stop();
        //else
        //    _airParticles.Play();

        //UpdateCameraRoot(1);
    }

    private void KillCameraRootSeq() => _cameraRootSeq.Kill();
    private void StartMovement() => _stopMovement = false;
    private void StopMovement() => _stopMovement = true;

    #region Shake

    private void ShakeCamera()
    {
        _cameraShakeTransform.DOKill();
        _cameraShakeTransform.DOShakePosition(0.5f, 0.5f, 10).OnComplete(delegate
        {
            _cameraShakeTransform.localPosition = Vector3.zero;
        });
    }

    private void ResetShake()
    {
        _cameraShakeTransform.DOKill();
        _cameraShakeTransform.localPosition = Vector3.zero;
    }

    #endregion Shake
}
