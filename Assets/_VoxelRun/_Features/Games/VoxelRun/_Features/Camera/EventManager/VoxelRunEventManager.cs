using MSKit.Events;

public static partial class EventManager
{
    public static Observable OnShakeCamera = new Observable(ObserverType.Default);
    public static void DoFireShakeCameraEvent() => OnShakeCamera.Fire();
}