using UnityEngine;

public partial class GameConfig : ScriptableObject
{
    [Header("Platform")]
    public float SinglePlatformLength;
    public int MinMidPlatformsToSpawn;
    public int MaxMidPlatformsToSpawn;
    public ValueRange GapInPlatformRange;
    public ValueRange ConnectorXRange;
    public float DualConnectorProbability;
}
