﻿using System.Collections.Generic;
//using MSKit.Configurations;
using MSKit.ResourcePool;
using UnityEngine;

public class VRPlatformController : MonoBehaviour
{
    [SerializeField] private GameConfig _configs;
    //[SerializeField] private ParticleSystem _confetti;
    [SerializeField] private Transform _finalPlatform;

    [SerializeField] private Transform _startPlatform;
    [SerializeField] private Transform _endPlatform;
    [SerializeField] private Transform _midPlatform;
    [SerializeField] private Transform _connectors;

    [SerializeField] private Transform _poolContainer;

    private readonly List<Vector2> _points = new List<Vector2>();
    private readonly Dictionary<int, Pool<Transform>> _platformPools = new Dictionary<int, Pool<Transform>>();
    private readonly InterpolationTable _interpolationTable = new InterpolationTable();

    public void Initialize()
    {
        //EventManager.OnPlayConfetti += PlayConfetti;
        _platformPools.Add(0, new Pool<Transform>(_poolContainer, _startPlatform, 10, 10));
        _platformPools.Add(1, new Pool<Transform>(_poolContainer, _endPlatform, 10, 10));
        _platformPools.Add(2, new Pool<Transform>(_poolContainer, _midPlatform, 20, 30));
        _platformPools.Add(3, new Pool<Transform>(_poolContainer, _connectors, 10, 15));
    }

    public void LoadGame(bool generateNewLevel, float levelLength)
    {
        if (!generateNewLevel) return;

        var data = SpawnPlatforms(levelLength);
        SpawnFinalPlatform(data);

        EventManager.DoFireAddLevelsDataEvent(data);
    }

    public void UnloadGame()
    {
        foreach (var pool in _platformPools)
        {
            pool.Value.Reset();
        }
    }

    private VRPlatformData SpawnPlatforms(float levelLength)
    {
        var levelData = new VRPlatformData();
        var singlePlatformLength = _configs.SinglePlatformLength;
        var platformsToSpawn = (int)(levelLength / singlePlatformLength);
        var minMidPlatforms = _configs.MinMidPlatformsToSpawn;
        var maxMidPlatforms = _configs.MaxMidPlatformsToSpawn;
        var currentPoint = 0f;
        var platformSize = Vector3.one;
        var midPointsToSpawn = 0;

        _points.Clear();
        _points.Add(Vector2.zero);

        for (int i = 0; i < platformsToSpawn; i++)
        {
            if (midPointsToSpawn == 0)
            {
                midPointsToSpawn = Random.Range(minMidPlatforms, maxMidPlatforms);
                SpawnPlatform(0, Vector3.forward * currentPoint, platformSize);
                currentPoint += singlePlatformLength;

                if (i + midPointsToSpawn >= platformsToSpawn)
                {
                    midPointsToSpawn = platformsToSpawn - i;
                }

                continue;
            }
            else if (midPointsToSpawn == 1)
            {
                SpawnPlatform(1, Vector3.forward * currentPoint, platformSize);
                --midPointsToSpawn;

                if (i != platformsToSpawn - 1)
                {
                    _points.Add(Vector2.up * (currentPoint + singlePlatformLength * 0.5f));
                    var connectorLength = AddConnector(ref currentPoint);
                    currentPoint += connectorLength;
                    _points.Add(Vector2.up * (currentPoint + connectorLength * 0.5f));
                }
                else
                {
                    currentPoint += singlePlatformLength;
                }

                continue;
            }
            else
            {
                SpawnPlatform(2, Vector3.forward * currentPoint, platformSize);
                currentPoint += singlePlatformLength;
                --midPointsToSpawn;
                continue;
            }
        }

        currentPoint -= 2.1f;
        var lastPoint = _points.Last();

        if (lastPoint.y > currentPoint)
        {
            _points.RemoveLast();
            var newPoint = currentPoint - 2.1f;
            _points.Add(Vector2.up * newPoint);
        }

        _points.Add(Vector2.up * currentPoint);

        levelData.Path = _points;

        return levelData;
    }

    private float AddConnector(ref float currentPoint)
    {
        var singlePlatformLength = _configs.SinglePlatformLength;
        var gapInBuildingsRange = _configs.GapInPlatformRange;
        var connectorXRange = _configs.ConnectorXRange;
        var gap = Random.Range(gapInBuildingsRange.MinValue, gapInBuildingsRange.MaxValue);
        var connectorPoint = gap * 0.5f + currentPoint;
        var connectorScale = gap - singlePlatformLength;
        var spawnDualConnectors = Random.value < _configs.DualConnectorProbability;

        if(spawnDualConnectors)
        {
            var xPos = connectorXRange.MaxValue;
            SpawnPlatform(3, new Vector3(xPos, 0, connectorPoint), new Vector3(1, 1, connectorScale));
            SpawnPlatform(3, new Vector3(-xPos, 0, connectorPoint), new Vector3(1, 1, connectorScale));
        }
        else
        {
            var xPos = Random.Range(connectorXRange.MinValue, connectorXRange.MaxValue);
            SpawnPlatform(3, new Vector3(xPos, 0, connectorPoint), new Vector3(1, 1, connectorScale));
        }

        return gap;
    }

    private void SpawnPlatform(int platformId, Vector3 position, Vector3 size)
    {
        var platform = _platformPools[platformId].Get();
        platform.localPosition = position;
        platform.localScale = size;
        platform.gameObject.SetActive(true);
    }

    private void SpawnFinalPlatform(VRPlatformData pathData)
    {
        _interpolationTable.InitializePoints(pathData.Path.ConvertToVector3());

        var evalPoint = _interpolationTable.Evaluate(1);
        var rotation = Quaternion.Euler(evalPoint.Tangent).eulerAngles;

        rotation.z = 0;
        _finalPlatform.position = evalPoint.Point;
        _finalPlatform.eulerAngles = rotation;
    }

    //private void PlayConfetti() => _confetti.Play(true);
}
