﻿using System.Collections.Generic;
//using MSKit.Configurations;
using MSKit.ResourcePool;
using UnityEngine;

public class VRNpcController : MonoBehaviour
{
    [SerializeField] private GameConfig _configs;
    [SerializeField] private VRNpc _npcObj;
    [SerializeField] private Transform _container;

    private ValueRange SpawnGap;

    private Pool<VRNpc> _npcPool;

    private readonly List<VRNpc> _npcList = new List<VRNpc>();
    private readonly List<Color> _npcColors = new List<Color>();
    private readonly List<PlayerRacePositionModel> _npcRacePositionModelsList = new List<PlayerRacePositionModel>();

    private NpcRacePositionsModel _npcRacePositionsModel = new NpcRacePositionsModel();
    protected readonly InterpolationTable _interpolationTable = new InterpolationTable();


    public void Initialize()
    {
        _npcPool = new Pool<VRNpc>(_container, _npcObj, 5, 15);

        RegisterEvents();
    }

    public void LoadGame(bool generateNewLevel, int npcToSpawn)
    {
        SpawnNpc(generateNewLevel, npcToSpawn);
    }

    public void StartGame()
    {
        for (int i = 0; i < _npcList.Count; i++)
        {
            _npcList[i].StartGame();
        }
    }

    public void UnloadGame()
    {
        for (int i = 0; i < _npcList.Count; i++)
        {
            _npcList[i].UnloadGame();
        }

        _npcPool.Reset();
    }

    private void RegisterEvents()
    {
        EventManager.OnGetNpcRacePositionsModel += GetNpcRacePositionsModel;
        EventManager.OnSetNpcRacePositionsModel += SetNpcRacePositionsModel;
    }

    private void SpawnNpc(bool generateNewLevel, int npcToSpawn)
    {
        _npcList.Clear();

        var toSpawn = npcToSpawn;
        var currentGapValue = 0f;

        if (generateNewLevel)
        {
            var levelData = EventManager.DoFireGetCurrentPlatformDataEvent();

            CalculateSpawnGap();
            _interpolationTable.InitializePoints(levelData.Path.ConvertToVector3());
        }

        for (int i = 0; i < toSpawn; i++)
        {
            currentGapValue += Random.Range(SpawnGap.MinValue, SpawnGap.MaxValue);
            var npc = _npcPool.Get();
            npc.gameObject.SetActive(true);
            npc.Load(generateNewLevel, currentGapValue, GetColorToAssign(), i + 1, _interpolationTable);
            _npcList.Add(npc);
        }
    }

    private void InitializeColorsList()
    {
        var colors = _configs.NpcColors;
        _npcColors.AddRange(colors);
    }

    private Color GetColorToAssign()
    {
        if (_npcColors.Count == 0) InitializeColorsList();

        var colorIndex = Random.Range(0, _npcColors.Count);
        var color = _npcColors[colorIndex];

        _npcColors.RemoveAt(colorIndex);

        return color;
    }

    private void SetNpcRacePositionsModel(NpcRacePositionsModel models)
    {
        for (int i = 0; i < models.RaceModels.Count; i++)
        {
            var model = models.RaceModels[i];
            var id = model.PlayerId;
            if (id == 0) continue;

            _npcList[id - 1].SetPlayerRacePosition(model);
        }
    }

    private NpcRacePositionsModel GetNpcRacePositionsModel()
    {
        _npcRacePositionModelsList.Clear();

        for (int i = 0; i < _npcList.Count; i++)
        {
            _npcRacePositionModelsList.Add(_npcList[i].GetPlayerRacePositionModel());
        }

        _npcRacePositionsModel.RaceModels = _npcRacePositionModelsList;

        return _npcRacePositionsModel;
    }

    private void CalculateSpawnGap()
    {
        SpawnGap = new ValueRange();
        var npcGap = _configs.NpcSpawnGap;
        var levelLength = EventManager.DoFireGetPlatformLengthEvent();
        var minGap = levelLength / npcGap.MinValue;
        var maxGap = levelLength / npcGap.MaxValue;

        SpawnGap.MinValue = 1f / minGap;
        SpawnGap.MaxValue = 1f / maxGap;
    }
}
