﻿using System.Collections;
//using MSKit.Configurations;
using UnityEngine;
using DG.Tweening;
using TMPro;

public class VRNpc : VRBaseCharacterController
{
    [SerializeField] private Transform[] _bonesTransform;
    [SerializeField] private Transform _raycastPoint;
    //[SerializeField] private TextMeshPro _positionText;
    //[SerializeField] private TextMeshPro _nameText;
    //[SerializeField] private SpriteRenderer _countryIcon;
    //[SerializeField] private RectTransform _countryTransform;
    //[SerializeField] private RagdollController _ragdollController;
    //[SerializeField] private Transform _characterDetailsTransform;

    private WaitForSeconds _hurdleRaycastWait;
    private WaitForSeconds _npcBehaviourWait;
    private WaitForSeconds _resumeBehaviourWait;
    private Coroutine _npcBehaviourCR;
    private Coroutine _resumeBehaviourCR;
    private Tween _dragTween;

    private bool _holdHurdleDetection;

    private float _minGapFromPlayerToDie;
    private float _minDistanceFromPlayer;
    private float _maxDistanceFromPlayer;
    private float _boostInterpolationSpeed;


    public override void Initialize()
    {
        if (_IsInitialized) return;

        base.Initialize();

        _hurdleRaycastWait = new WaitForSeconds(_configs.NpcHurdleRaycastDelay);
        _npcBehaviourWait = new WaitForSeconds(_configs.NpcBehaviourDelay);
        _resumeBehaviourWait = new WaitForSeconds(_configs.NpcResumeBehaviourWait);

        _PlayerDistancePerSecond = _configs.PlayerDistancePerSecond;
        CalculateInterpolationSpeed();
        _boostInterpolationSpeed = _InterpolationSpeed;

        //_ragdollController.Initialize();
    }

    public override void LoadGame(bool generateNewLevel)
    {
        ResetCharacterDetailsRotation();
        //_ragdollController.ResetRagdoll();

        base.LoadGame(generateNewLevel);

        var bounds = _configs.PlayerRightBounds;
        RightPosition = Random.Range(bounds.MinValue, bounds.MaxValue);

        _holdHurdleDetection = false;

        var speedRange = _configs.NpcNormalDistancePerSecondRange;
        _PlayerDistancePerSecond = Random.Range(speedRange.MinValue, speedRange.MaxValue);
        CalculateGapsFromPlayerToDie();
    }

    public override void StartGame()
    {
        base.StartGame();

        _npcBehaviourCR = StartCoroutine(NpcBehaviourCR());
    }

    public override void UnloadGame()
    {
        base.UnloadGame();

        StopBehaviourCR();
        StopResumeBehaviourCR();
    }

    public void Load(bool generateNewLevel, float gap, Color color, int npcId, InterpolationTable interpolationTable)
    {
        _interpolationTable = interpolationTable;
        Initialize();
        ForceTurnToSolid();
        LoadGame(generateNewLevel);
        SetSpawnPosition(gap);
        AssignColor(color);
        LoadPlayerInformation();
        _PlayerId = npcId;
    }

    public override void SetPlayerRacePosition(PlayerRacePositionModel model)
    {
        //base.SetPlayerRacePosition(model);

        //var position = _PlayerRacePosition +
        //    (_PlayerRacePosition == 1 ? "st" : _PlayerRacePosition == 2 ? "nd" : _PlayerRacePosition == 3 ? "rd" : "th");

        //_positionText.text = position;
    }

    protected override void FingerUp()
    {
        base.FingerUp();
        _InterpolationSpeed = _boostInterpolationSpeed;
    }

    protected override void FinishLineReached()
    {
        base.FinishLineReached();

        StopBehaviourCR();
        StopResumeBehaviourCR();
        FlipCharacterDetails();
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();

        AvoidHurdle();
    }

    protected override void Collapsed()
    {
        base.Collapsed();
        StopBehaviourCR();
        StopResumeBehaviourCR();

        //_ragdollController.ApplyRagdoll(_MovingObj.position, _MovingObj.rotation, _bonesTransform, PlayerColor);

        _resumeBehaviourCR = StartCoroutine(CheckToResumeBehaviourCR());
    }

    private void AvoidHurdle()
    {
        if (_holdHurdleDetection || _StopMovement) return;

        if (Physics.Raycast(_raycastPoint.position, _raycastPoint.forward, out RaycastHit hit, _configs.NpcHurdleRaycastDistance, _configs.NpcHurdleLayerMask))
        {
            var dir = hit.transform.localPosition.x < 0 ? 1 : -1;
            MoveRandomRight(dir);
            FingerDown();
        }

        _holdHurdleDetection = true;

        StartCoroutine(HoldHurdleDetectionCR());
    }

    private void MoveRandomRight(float dir)
    {
        var dragRange = _configs.NpcDragRange;
        var dragAmount = Random.Range(dragRange.MinValue, dragRange.MaxValue);
        var currentDrag = RightPosition;
        var time = _configs.NpcDragTime;

        dragAmount *= dir;

        _dragTween.Kill();
        _dragTween = DOTween.To(() => currentDrag, x => currentDrag = x, dragAmount, time)
            .OnUpdate(() =>
            {
                var drag = Vector3.right * currentDrag;
                PlayerDrag(drag);
            });
    }

    private IEnumerator NpcBehaviourCR()
    {
        yield return _npcBehaviourWait;

        if (_StopMovement) yield break;

        var isBoostApplied = AdjustSpeedBasedOnPlayer();

        if (!isBoostApplied)
        {
            var randomValue = Random.value;
            if (randomValue < _configs.NpcConversionToVoxelProb)
            {
                if (_IsFingerUp) FingerDown();
                else FingerUp();
            }
            else
            {
                FingerDown();
                var dir = randomValue < 0.65f ? 1 : -1;
                MoveRandomRight(dir);
            }
        }

        _npcBehaviourCR = StartCoroutine(NpcBehaviourCR());
    }

    private void StopBehaviourCR()
    {
        if (_npcBehaviourCR != null)
        {
            StopCoroutine(_npcBehaviourCR);
            _npcBehaviourCR = null;
        }
    }

    private IEnumerator HoldHurdleDetectionCR()
    {
        yield return _hurdleRaycastWait;
        _holdHurdleDetection = false;
    }

    private void SetSpawnPosition(float gap)
    {
        _InterpolationValue = gap;

        ResetPlayerToInitialPosition();
        InitialScatter();
    }

    private void AssignColor(Color color) => _ColorController.SetColor(color);

    private void InitialScatter()
    {
        var rotation = Quaternion.LookRotation(_currentEvaluation.Tangent);
        var point = _currentEvaluation.Point + _MovingObj.right * RightPosition;

        ForceSetTransform(point, rotation);
    }

    private void LoadPlayerInformation()
    {
        //var playerInfo = EventManager.DoFireGetPlayerInformationEvent();

        //_nameText.text = playerInfo.Name;
        //_countryIcon.sprite = playerInfo.CountryIcon;
        //_countryTransform.anchoredPosition = Vector2.right * (0.06f - (0.06f * playerInfo.Name.Length));
    }

    private void ResetCharacterDetailsRotation()
    {
        //_characterDetailsTransform.localRotation = Quaternion.identity;
    }

    private void FlipCharacterDetails()
    {
        //_characterDetailsTransform.Rotate(Vector3.up, 180f);
    }

    private void CalculateGapsFromPlayerToDie()
    {
        var platformLength = EventManager.DoFireGetPlatformLengthEvent();
        var minGapToDieTime = platformLength / _configs.NpcDistanceFromPlayerToDie;
        var minDistanceFromPlayerTime = platformLength / _configs.NpcMinDistanceFromPlayer;
        var maxDistanceFromPlayerTime = platformLength / _configs.NpcMaxDistanceFromPlayer;

        _minGapFromPlayerToDie = 1f / minGapToDieTime;
        _minDistanceFromPlayer = 1f / minDistanceFromPlayerTime;
        _maxDistanceFromPlayer = 1f / maxDistanceFromPlayerTime;
    }

    public override void CollidedWithHurdle()
    {
        if (_StopMovement || IsNotInViewOfPlayer()) return;

        PauseBehaviour();
        _CharacterAnimationController.PlayAnimation(CharacterAnimationType.Death);
    }

    public override bool FallDown()
    {
        if (_StopMovement) return true;
        else if (IsNotInViewOfPlayer()) return false;
        else if (Random.value > 0.6f)
        {
            FingerUp();
            return false;
        }

        PauseBehaviour();
        _CharacterAnimationController.PlayAnimation(CharacterAnimationType.Fall);

        _MovingRb.AddForce(new Vector3(0, -5, 15), ForceMode.Impulse);

        return true;
    }

    private bool IsNotInViewOfPlayer()
    {
        var playerInterpolationValue = EventManager.DoFireGetPlayerInterpolationValueEvent();

        return _InterpolationValue < playerInterpolationValue
            || _InterpolationValue - playerInterpolationValue > _minGapFromPlayerToDie;
    }

    private void PauseBehaviour()
    {
        _StopMovement = true;
        //_JetPackController.HideParticles();
        SetPhysicsState(true);
        StopBehaviourCR();
        _resumeBehaviourCR = StartCoroutine(CheckToResumeBehaviourCR());
    }

    private void ResumeBehaviour()
    {
        _StopMovement = false;
        SetCharacterState(true);
        _MovingRb.velocity = Vector2.zero;
        ResetPlayerToInitialPosition();
        _npcBehaviourCR = StartCoroutine(NpcBehaviourCR());
        _CharacterAnimationController.PlayAnimation(CharacterAnimationType.Run);
    }

    private IEnumerator CheckToResumeBehaviourCR()
    {
        yield return _resumeBehaviourWait;

        var playerInterpolationValue = EventManager.DoFireGetPlayerInterpolationValueEvent();

        if (playerInterpolationValue > _InterpolationValue)
        {
            ResumeBehaviour();
        }
        else
        {
            _resumeBehaviourCR = StartCoroutine(CheckToResumeBehaviourCR());
        }
    }

    private void StopResumeBehaviourCR()
    {
        if (_resumeBehaviourCR != null)
        {
            StopCoroutine(_resumeBehaviourCR);
            _resumeBehaviourCR = null;
        }
    }

    private bool AdjustSpeedBasedOnPlayer()
    {
        var playerInterpolationValue = EventManager.DoFireGetPlayerInterpolationValueEvent();
        var speedRange = _configs.NpcNormalDistancePerSecondRange;
        var isBoostApplied = false;

        if (_InterpolationValue < playerInterpolationValue && playerInterpolationValue - _InterpolationValue > _minDistanceFromPlayer)
        {
            var convertToVoxel = Random.value < _configs.NpcBoostProbToCatchUpPlayer;
            if (convertToVoxel)
            {
                speedRange = _configs.NpcMaxDistancePerSecondRange;
                //_JetPackController.SetFuel(1f);
                ApplyPlayerScale();
                FingerUp();
            }
            isBoostApplied = true;
        }
        else if(_InterpolationValue > playerInterpolationValue && _InterpolationValue - playerInterpolationValue > _maxDistanceFromPlayer)
        {
            speedRange = _configs.NpcMinDistancePerSecondRange;
        }

        _PlayerDistancePerSecond = Random.Range(speedRange.MinValue, speedRange.MaxValue);
        CalculateInterpolationSpeed();

        return isBoostApplied;
    }

    protected override void FinalPointReached() => PlayIdleAnimation();

    protected override void ApplyPlayerScale()
    {
        
    }
}

