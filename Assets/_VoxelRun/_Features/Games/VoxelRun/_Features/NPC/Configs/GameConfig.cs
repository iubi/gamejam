using System.Collections.Generic;
using UnityEngine;

public partial class GameConfig : ScriptableObject
{
    [Header("NPC")]

    public ValueRange NpcSpawnGap;
    public ValueRange NpcDragRange;
    public int NpcToSpawn;
    public ValueRange NpcNormalDistancePerSecondRange;
    public ValueRange NpcMaxDistancePerSecondRange;
    public ValueRange NpcMinDistancePerSecondRange;
    public float NpcHurdleRaycastDistance;
    public float NpcHurdleRaycastDelay;
    public float NpcDragTime;
    public float NpcBehaviourDelay;
    public float NpcResumeBehaviourWait;
    public float NpcConversionToVoxelProb;
    public float NpcDistanceFromPlayerToDie;
    public float NpcMaxDistanceFromPlayer;
    public float NpcMinDistanceFromPlayer;
    public float NpcBoostProbToCatchUpPlayer;
    public LayerMask NpcHurdleLayerMask;
    public List<Color> NpcColors;
}
