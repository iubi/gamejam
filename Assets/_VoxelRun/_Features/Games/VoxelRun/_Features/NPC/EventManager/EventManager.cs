using System.Collections.Generic;
using MSKit.Events;

public static partial class EventManager
{
    public static Observable<NpcRacePositionsModel> OnGetNpcRacePositionsModel = new Observable<NpcRacePositionsModel>(ObserverType.Callback);
    public static NpcRacePositionsModel DoFireGetNpcRacePositionsModelEvent() => OnGetNpcRacePositionsModel.Fire();

    public static Observable<NpcRacePositionsModel> OnSetNpcRacePositionsModel = new Observable<NpcRacePositionsModel>(ObserverType.Parameter);
    public static void DoFireSetNpcRacePositionsModelEvent(NpcRacePositionsModel model) => OnSetNpcRacePositionsModel.Fire(model);

}

public struct NpcRacePositionsModel
{
    public List<PlayerRacePositionModel> RaceModels;
}