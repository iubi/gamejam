using UnityEngine;

[CreateAssetMenu(fileName = "GameConfigs", menuName = "Create Game Configs", order = 0)]
public partial class GameConfig : ScriptableObject
{
    [Header("Player")]
    public float PlayerDistancePerSecond;
    public float PlayerMovementSpeed;
    public float PlayerRotationSpeed;
    public float PlayerAccelerationSpeed;
    public float PlayerInitialAcceleration;
    public float PlayerDragSpeed;
    public float PlayerMaxDragRotation;
    public float PlayerBoostInVoxel;
    public float PlayerBoostInitialImpact;
    public float PlayerBoostImpactOverSpeed;
    public float PlayerDeteriorationRate;
    public float PlayerMinFuelToConvert;
    public float PlayerInitialFuel;
    public float PlayerCollapseStartTime;
    public float PlayerFlyHeight;

    public ValueRange PlayerRightBounds;

    public Color PlayerColor;
}

[System.Serializable]
public struct ValueRange
{
    public float MinValue;
    public float MaxValue;
}