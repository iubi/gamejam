﻿using UnityEngine;

public class VRPlayerTransitionController : VRBaseTransitionController
{
    [SerializeField] ParticleSystem _speedParticles;

    public override void TransitionToVoxel()
    {
        base.TransitionToVoxel();
        ShowSpeedParticles();
    }

    public override void TransitionToSolid()
    {
        base.TransitionToSolid();
        HideSpeedParticles();
    }

    private void ShowSpeedParticles() => _speedParticles.Play();
    private void HideSpeedParticles()
    {
        _speedParticles.Stop();
        _speedParticles.Clear();
    }
}
