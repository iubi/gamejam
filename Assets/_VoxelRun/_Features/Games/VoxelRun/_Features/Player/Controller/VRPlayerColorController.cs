﻿using UnityEngine;

public class VRPlayerColorController : MonoBehaviour
{
    [SerializeField] protected SkinnedMeshRenderer _PlayerHead;
    [SerializeField] protected SkinnedMeshRenderer _PlayerBodyRenderer;

    private MaterialPropertyBlock _materialPropertyBlock;
    private int _colorId;
    private Color _color;

    public Color Color { get { return _color; } }

    public void Initialize()
    {
        _colorId = Shader.PropertyToID("_Color");
        _materialPropertyBlock = new MaterialPropertyBlock();
        _PlayerBodyRenderer.GetPropertyBlock(_materialPropertyBlock);
    }

    public void SetColor(Color color)
    {
        _color = color;
        _materialPropertyBlock.SetColor(_colorId, _color);
        _PlayerHead.SetPropertyBlock(_materialPropertyBlock);
        _PlayerBodyRenderer.SetPropertyBlock(_materialPropertyBlock);
    }

    public void ShowGlow()
    {
        _PlayerHead.material.EnableKeyword("BOOLEAN_A21E3204_ON");
        _PlayerBodyRenderer.material.EnableKeyword("BOOLEAN_A21E3204_ON");
    }

    public void HideGlow()
    {
        _PlayerHead.material.DisableKeyword("BOOLEAN_A21E3204_ON");
        _PlayerBodyRenderer.material.DisableKeyword("BOOLEAN_A21E3204_ON");
    }
}
