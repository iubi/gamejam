﻿using System.Collections.Generic;
using DG.Tweening;
//using MoreMountains.NiceVibrations;
//using MSKit.Configurations;
//using MSKit.Database;
//using MSKit.Hatpics;
using UnityEngine;

public class VRPlayerController : VRBaseCharacterController
{
    [SerializeField] private SphereCollider _headCollider;
    [SerializeField] private Transform _scaleObj;
    private Sequence _scaleTween;
    private int _finalRacePosition;

    private void RegisterEvents()
    {
        EventManager.OnInputDrag += PlayerDrag;
        EventManager.OnFingerDown += FingerDown;
        EventManager.OnFingerUp += FingerUp;
        //VoxelRunEventManager.OnCollectableCollected += CollectableCollected;
        //VoxelRunEventManager.OnGetPlayerRacePositionModel += GetPlayerRacePositionModel;
        //VoxelRunEventManager.OnSetPlayerRacePositionModel += SetPlayerRacePosition;
        //VoxelRunEventManager.OnGetPlayerInterpolationValue += GetInterpolationValue;
        //VoxelRunEventManager.OnGetPlayerFinalRacePosition += GetPlayerFinalRacePosition;
    }

    public override void Initialize()
    {
        base.Initialize();

        _PlayerDistancePerSecond = _configs.PlayerDistancePerSecond;

        RegisterEvents();
    }

    public override void LoadGame(bool generateNewLevel)
    {
        base.LoadGame(generateNewLevel);

        if (generateNewLevel)
        {
            var levelData = EventManager.DoFireGetCurrentPlatformDataEvent();
            _interpolationTable.InitializePoints(levelData.Path.ConvertToVector3());
            CalculateInterpolationSpeed();
        }

        var playerColor = _configs.PlayerColor;
        _ColorController.SetColor(playerColor);
        _ColorController.HideGlow();

        RightPosition = 0;
        _InterpolationValue = 0.002f;
        ResetPlayerToInitialPosition();
        SetHeadColliderRadius(0.2f);
    }

    public override void StartGame()
    {
        base.StartGame();

        //_JetPackController.RandomizeNoise();
    }

    public override void UnloadGame()
    {
        base.UnloadGame();

        //_JetPackController.StopNoise();
    }

    public override void CollidedWithHurdle()
    {
        if (_StopMovement) return;

        //VoxelRunEventManager.DoFireShakeCameraEvent();
        GameEnd();
        _CharacterAnimationController.PlayAnimation(CharacterAnimationType.Death);
    }

    public override bool FallDown()
    {
        if (_StopMovement) return true;

        SetHeadColliderRadius(0f);
        GameEnd();
        _CharacterAnimationController.PlayAnimation(CharacterAnimationType.Fall);

        _MovingRb.AddForce(new Vector3(0, -5, 15), ForceMode.Impulse);

        return true;
    }

    private void GameEnd()
    {
        _StopMovement = true;
        EventManager.DoFireDeactivateInputEvent();
        //_JetPackController.HideParticles();
        SetPhysicsState(true);
        SetHeadColliderRadius(0.2f);
        //EventManager.DoFireFailGameEvent();
        Debug.Log("GAME OVER");

        ForceTurnToSolid();

        //if (HapticState.GetHapticState())
        //{
        //    MMVibrationManager.StopContinuousHaptic();
        //    Vibration.Vibrate(TapticPlugin.ImpactFeedback.Medium);
        //}
    }

    public override void SetPlayerRacePosition(PlayerRacePositionModel model)
    {
        base.SetPlayerRacePosition(model);

        EventManager.DoFireSetPlayerRacePositionEvent(_PlayerRacePosition);
    }

    protected override void Move()
    {
        base.Move();

        EventManager.DoFirePlayerPointUpdateEvent(_currentEvaluation);
    }

    protected override void TransitionToSolid(bool isDeath = false)
    {
        //base.TransitionToSolid(isDeath);

        //SetHeadColliderRadius(0.2f);
        //_ColorController.HideGlow();
        //EventManager.DoFirePlayerStateChangeEvent(PlayerState);

        //if (HapticState.GetHapticState())
        //{
        //    MMVibrationManager.StopContinuousHaptic();
        //}
    }

    protected override void TransitionToVoxels()
    {
        //base.TransitionToVoxels();

        ////var convertedScale = _JetPackController.Fuel;
        ////if (convertedScale <= 0) return;

        //SetHeadColliderRadius(0.5f);
        //_ColorController.ShowGlow();
        //EventManager.DoFirePlayerStateChangeEvent(PlayerState);

        //if (HapticState.GetHapticState())
        //{
        //    MMVibrationManager.ContinuousHaptic(0.1f, 0.1f, 5, HapticTypes.LightImpact, this);
        //}
    }

    protected override void Deteriorate()
    {
        base.Deteriorate();

        //EventManager.DoFirePlayerScaleUpdateEvent(_JetPackController.Fuel);
    }

    protected override void ApplyPlayerScale()
    {
        ResetScaleSeq();

        //VoxelRunEventManager.DoFirePlayerScaleUpdateEvent(_JetPackController.Fuel);
    }

    protected override void FinishLineReached()
    {
        if (_StopMovement) return;

        base.FinishLineReached();

        _finalRacePosition = _PlayerRacePosition;

        //if (HapticState.GetHapticState())
        //{
        //    MMVibrationManager.StopContinuousHaptic();
        //}

        EventManager.DoFireDeactivateInputEvent();
        //EventManager.DoFirePlayConfettiEvent();

        //Vibration.Vibrate(TapticPlugin.ImpactFeedback.Medium);
    }

    protected override void FinalPointReached()
    {
        //EventManager.DoFireGameCompleteEvent();
        PlayVictoryAnimation();

        Debug.Log("PLAYER REACHED FINAL POSITION");
    }

    protected override void ResetPlayerToInitialPosition()
    {
        base.ResetPlayerToInitialPosition();

        EventManager.DoFirePlayerPointUpdateEvent(_currentEvaluation);
    }

    //public override void ConsumeOpponent(List<Voxel> voxels)
    //{
    //    base.ConsumeOpponent(voxels);

    //    if (HapticState.GetHapticState())
    //    {
    //        MMVibrationManager.StopContinuousHaptic();
    //        MMVibrationManager.ContinuousHaptic(0.1f, 0.1f, 5, HapticTypes.LightImpact, this);
    //    }
    //    //VoxelRunEventManager.DoFireShakeCameraEvent();
    //}

    private void CollectableCollected()
    {
        _ColorController.ShowGlow();

        ResetScaleSeq();

        _scaleTween
            .Append(_scaleObj.DOScale(1 + 0.1f, 0.03f).SetEase(Ease.OutSine))
            .Append(_scaleObj.DOScale(1, 0.03f).SetEase(Ease.InSine))
            .OnComplete(() =>
            {
                _ColorController.HideGlow();
            });
    }

    private void ResetScaleSeq()
    {
        _scaleTween?.Kill();
        _scaleTween = DOTween.Sequence();
    }

    private void SetHeadColliderRadius(float radius) => _headCollider.radius = radius;
    private float GetInterpolationValue() => _InterpolationValue;
    public int GetPlayerFinalRacePosition() => _finalRacePosition;

}
