﻿using System.Collections.Generic;
using UnityEngine;

public abstract class VRBaseTransitionController : MonoBehaviour
{
    [SerializeField] protected GameObject _PlayerBody;
    [SerializeField] protected SkinnedMeshRenderer _PlayerHead;
    //[SerializeField] protected BaseVoxelController _VoxelController;

    public void Initialize()
    {
        //_VoxelController.Initialize();
    }

    public virtual void Load()
    {
       
    }

    public virtual void TransitionToVoxel()
    {

    }

    public virtual void TransitionToSolid()
    {

    }

    //public void ForceTransitionToSolid() => _VoxelController.ForceHideVoxels();

    //public List<Voxel> Collapse(float scale, float speed) => _VoxelController.CollapseVoxels(scale, speed);
    //public void Consume(List<Voxel> voxels, float speed) => _VoxelController.ConsumeVoxels(voxels, speed);
}

