﻿using System.Collections.Generic;
using DG.Tweening;
//using MSKit.Configurations;
using UnityEngine;

public abstract class VRBaseCharacterController : MonoBehaviour
{
    [SerializeField] protected GameConfig _configs;
    [SerializeField] protected GameObject _CharacterObj;
    [SerializeField] protected Transform _BaseTransform;
    [SerializeField] protected Collider _Collider;
    [SerializeField] protected Animator _Animator;
    [SerializeField] protected Transform _MovingObj;
    [SerializeField] protected Rigidbody _MovingRb;

    [SerializeField] protected VRBaseTransitionController _TransitionController;
    [SerializeField] protected VRPlayerColorController _ColorController;
    //[SerializeField] protected JetPackController _JetPackController;
    [SerializeField] protected bool _IsNpc;

    protected CharacterAnimationController _CharacterAnimationController;

    protected int _PlayerId;
    protected int _PlayerRacePosition;

    protected float _InterpolationSpeed;
    protected float _Acceleration;
    protected float _InterpolationValue;
    protected float _BoostMultiplier;
    protected float _TargetBoost;
    protected float _PlayerDistancePerSecond;

    protected bool _StopMovement = true;
    protected bool _IsInitialized;
    protected bool _IsFingerUp;

    protected EvalPoint _currentEvaluation;

    public VRPlayerState PlayerState { get; protected set; }
    public Vector3 PlayerPredictedPosition { get { return _MovingObj.position + _MovingObj.forward * 3f + _MovingObj.up; } }
    public Color PlayerColor { get { return _ColorController.Color; } }
    public float PlayerBoostSpeed { get { return _BoostMultiplier / _TargetBoost; } }

    public float RightPosition { get; protected set; }
    public float DragRotationDelta { get; protected set; }
    public float BoostPositionDelta { get; private set; }
    public bool IsNpc { get { return _IsNpc; } }

    protected InterpolationTable _interpolationTable = new InterpolationTable();
    private PlayerRacePositionModel _playerRacePositionModel = new PlayerRacePositionModel();

    #region Transition

    protected void FingerDown()
    {
        if (_IsFingerUp)
        {
            _IsFingerUp = false;
            //TransitionToSolid();
        }
    }

    protected virtual void FingerUp()
    {
        //if (_IsFingerUp) return;

        //TransitionToVoxels();
    }

    protected virtual void TransitionToSolid(bool isDeath = false)
    {
        PlayerState = VRPlayerState.Solid;
        TranisitionToSolidComplete();
        _TargetBoost = 1;

        ApplyPlayerScale();

        BoostPositionDelta = 0;

        SetBaseTransformHeight(0, false);
    }

    protected virtual void TransitionToVoxels()
    {
        //var convertedScale = _JetPackController.Fuel;

        //_IsFingerUp = convertedScale > 0;

        //if (!_IsFingerUp) return;

        _CharacterAnimationController.PlayAnimation(CharacterAnimationType.DiveStart);
        PlayerState = VRPlayerState.Voxel;
        _TargetBoost = _configs.PlayerBoostInVoxel;

        BoostPositionDelta = _configs.PlayerBoostInitialImpact;

        TranisitionToVoxelComplete();
        SetBaseTransformHeight(_configs.PlayerFlyHeight, !IsNpc);
    }

    private void TranisitionToSolidComplete()
    {
        ResetMovement();
        PlayDiveEndAnimation();
        SetPhysicsState(true);
        //_JetPackController.HideParticles();
    }

    private void TranisitionToVoxelComplete()
    {
        SetPhysicsState(false);
        ResetMovement();
        //_JetPackController.PlayParticles();
    }

    private void SetBaseTransformHeight(float targetHeight, bool animate)
    {
        var pos = _BaseTransform.localPosition;
        pos.y = targetHeight;

        if (!IsNpc) _BaseTransform?.DOKill();

        if (animate) _BaseTransform.DOLocalMove(pos, 0.2f).SetEase(Ease.OutQuad);
        else _BaseTransform.localPosition = pos;
    }

    private void ResetMovement()
    {
        _MovingRb.velocity = Vector2.zero;

        var pos = _MovingObj.position;
        pos.y = -0.0175f;
        _MovingObj.position = pos;
    }

    protected void ForceTurnToSolid()
    {
        SetBaseTransformHeight(0, false);
        //_TransitionController.ForceTransitionToSolid();
    }

    #endregion

    #region Animations

    private void PlayDiveEndAnimation() => _CharacterAnimationController.PlayAnimation(CharacterAnimationType.DiveEnd);
    protected void PlayVictoryAnimation() => _CharacterAnimationController.PlayAnimation(CharacterAnimationType.Victory);
    protected void PlayIdleAnimation() => _CharacterAnimationController.PlayAnimation(CharacterAnimationType.Idle);

    #endregion

    #region Lifecycle

    public virtual void Initialize()
    {
        if (_IsInitialized) return;

        _IsInitialized = true;
        _CharacterAnimationController = new CharacterAnimationController(_Animator);
        _TransitionController.Initialize();
        _ColorController.Initialize();
        //_JetPackController.Initialize();
    }

    public virtual void LoadGame(bool generateNewLevel)
    {
        SetCharacterState(true);
        _StopMovement = true;
        PlayIdleAnimation();

        _TransitionController.Load();

        //_JetPackController.SetFuel(Configs.VoxelRunGameConfigs.PlayerInitialFuel);
        ApplyPlayerScale();
        _MovingRb.velocity = Vector2.zero;

        _IsFingerUp = false;
        BoostPositionDelta = 0;
        _BoostMultiplier = 1;
        _TargetBoost = 1;
        _Acceleration = _configs.PlayerInitialAcceleration;
        PlayerState = VRPlayerState.Solid;

        SetPhysicsState(true);
    }

    public virtual void StartGame()
    {
        _CharacterAnimationController.PlayAnimation(CharacterAnimationType.Run);
        _StopMovement = false;
    }

    public void CompleteGame()
    {

    }

    public void FailGame()
    {

    }

    public virtual void UnloadGame()
    {
        _StopMovement = true;
    }

    protected virtual void FixedUpdate()
    {
        if (_StopMovement) return;

        Move();
        Deteriorate();
        CheckForPathEnd();
    }

    #endregion

    #region Movement

    protected void CalculateInterpolationSpeed()
    {
        var levelTime = EventManager.DoFireGetPlatformLengthEvent() / _PlayerDistancePerSecond;

        _InterpolationSpeed = 1f / levelTime;
    }

    protected virtual void Deteriorate()
    {
        if (PlayerState == VRPlayerState.Solid) return;

        //var isTankEmpty = _JetPackController.ConsumeFuel(!IsNpc);

        //if (isTankEmpty) TransitionToSolid(true);
    }

    protected virtual void ResetPlayerToInitialPosition()
    {
        _currentEvaluation = _interpolationTable.Evaluate(_InterpolationValue);

        var rotation = Quaternion.LookRotation(_currentEvaluation.Tangent);
        var point = _currentEvaluation.Point;

        ForceSetTransform(point, rotation);
    }

    protected void PlayerDrag(Vector3 drag)
    {
        var bounds = _configs.PlayerRightBounds;

        RightPosition += drag.x * Time.fixedDeltaTime * _configs.PlayerDragSpeed;
        RightPosition = Mathf.Clamp(RightPosition, bounds.MinValue, bounds.MaxValue);

        DragRotationDelta = drag.x * 50;
    }

    protected virtual void Move()
    {
        CalculateAcceleration();
        CalculateBoost();
        CalculateBoostDelta();

        var speed = _InterpolationSpeed * Time.fixedDeltaTime * _Acceleration * _BoostMultiplier;
        var movementSpeed = _configs.PlayerMovementSpeed;
        var rotationSpeed = _configs.PlayerRotationSpeed;

        _InterpolationValue += speed;

        _currentEvaluation = _interpolationTable.Evaluate(_InterpolationValue);
        var rotation = Quaternion.LookRotation(_currentEvaluation.Tangent);
        var boostDelta = _MovingObj.forward * BoostPositionDelta;
        var targetPosition = _currentEvaluation.Point + Vector3.right * RightPosition + boostDelta;
        var maxRotation = _configs.PlayerMaxDragRotation;
        var euler = rotation.eulerAngles;

        if (PlayerState == VRPlayerState.Solid)
        {
            var yPos = _MovingObj.position.y;

            if (yPos < -0.1f)
            {
                var isFalling = FallDown();
                if (!isFalling) yPos = 0;
            }

            targetPosition.y = yPos;
        }

        euler.y += DragRotationDelta;
        euler.y = Mathf.Clamp(euler.y, rotation.eulerAngles.y - maxRotation, rotation.eulerAngles.y + maxRotation);
        rotation.eulerAngles = euler;

        var nextPosition = Vector3.Lerp(_MovingRb.position, targetPosition, movementSpeed * Time.fixedDeltaTime);
        var nextRotation = Quaternion.Lerp(_MovingRb.rotation, rotation, rotationSpeed * Time.fixedDeltaTime);

        targetPosition -= Vector3.right * (RightPosition * 0.25f);
        targetPosition -= boostDelta;
        _currentEvaluation.Point = targetPosition;

        DragRotationDelta = 0;

        UpdatePlayerBody(nextPosition, nextRotation);
    }

    protected void UpdatePlayerBody(Vector3 point, Quaternion tangent)
    {
        _MovingRb.MovePosition(point);
        _MovingRb.MoveRotation(tangent);
    }

    protected void ForceSetTransform(Vector3 point, Quaternion tangent)
    {
        _MovingObj.position = point;
        _MovingObj.rotation = tangent;
    }

    private void CalculateAcceleration()
    {
        var speed = _configs.PlayerAccelerationSpeed;

        _Acceleration += speed * Time.fixedDeltaTime;
        _Acceleration = Mathf.Clamp(_Acceleration, 0, 1);
    }

    private void CalculateBoostDelta()
    {
        BoostPositionDelta -= _configs.PlayerBoostImpactOverSpeed * Time.fixedDeltaTime;
        BoostPositionDelta = Mathf.Clamp(BoostPositionDelta, 0, _configs.PlayerBoostInitialImpact);
    }

    private void CalculateBoost()
    {
        var speed = _configs.PlayerAccelerationSpeed;

        _BoostMultiplier += speed * Time.fixedDeltaTime;
        _BoostMultiplier = Mathf.Clamp(_BoostMultiplier, 0, _TargetBoost);
    }

    protected void SetPhysicsState(bool state)
    {
        _MovingRb.useGravity = state;
        _MovingRb.constraints = state ? RigidbodyConstraints.FreezeRotation : RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionY;
    }

    #endregion

    #region Scaling

    protected abstract void ApplyPlayerScale();

    #endregion

    #region Race

    private void CheckForPathEnd()
    {
        if (_InterpolationValue < 1) return;

        _InterpolationValue = 1.2f;
        FinishLineReached();
    }

    protected virtual void FinishLineReached()
    {
        if (_StopMovement) return;

        _StopMovement = true;

        if(PlayerState == VRPlayerState.Voxel)
        {
            //_JetPackController.HideParticles();
            PlayDiveEndAnimation();
            SetBaseTransformHeight(0, false);
            SetPhysicsState(false);
        }

        var targetPosition = _MovingObj.position + _MovingObj.forward * 6;
        var targetRotation = _MovingObj.eulerAngles + Vector3.up * 180;

        var seq = DOTween.Sequence()
            .Append(_MovingObj.DOMove(targetPosition, 0.5f))
            .Insert(0.3f, _MovingObj.DORotate(targetRotation, 0.22f))
            .InsertCallback(0.5f, FinalPointReached);

        _MovingRb.velocity = Vector2.zero;
    }

    public PlayerRacePositionModel GetPlayerRacePositionModel()
    {
        _playerRacePositionModel.PlayerId = _PlayerId;
        _playerRacePositionModel.InterpolationValue = _InterpolationValue;
        return _playerRacePositionModel;
    }

    public virtual void SetPlayerRacePosition(PlayerRacePositionModel model)
    {
        _playerRacePositionModel = model;
        _PlayerRacePosition = _playerRacePositionModel.PlayerRacePosition;
    }

    protected abstract void FinalPointReached();

    #endregion

    #region collisions

    public abstract void CollidedWithHurdle();
    public abstract bool FallDown();

    public void CollectVoxels(float voxels)
    {
        if (_StopMovement) return;

        var minfuelToConvert = _configs.PlayerMinFuelToConvert;

        //var fuel = Configs.VoxelRunGameConfigs.VoxelToPlayerScaleRatio * voxels;
        //_JetPackController.AddFuel(fuel);

        ApplyPlayerScale();

        //if (_IsFingerUp && _JetPackController.Fuel > minfuelToConvert) TransitionToVoxels();
    }

    //public List<Voxel> Collapse(float speed)
    //{
    //    Collapsed();

    //    var fuel = _JetPackController.Fuel;
    //    fuel = Mathf.Clamp(fuel, 0.25f, 1);

    //    var voxels = _TransitionController.Collapse(fuel, speed);

    //    return voxels;
    //}

    protected virtual void Collapsed()
    {
        _StopMovement = true;
        SetCharacterState(false);
    }

    //public virtual void ConsumeOpponent(List<Voxel> voxels)
    //{
    //     var fuel = Configs.VoxelRunGameConfigs.VoxelToPlayerScaleRatio * voxels.Count;
    //    _JetPackController.AddFuel(fuel);

    //    _TransitionController.Consume(voxels, PlayerBoostSpeed);

    //    ApplyPlayerScale();
    //}

    public bool CanCollect() => PlayerState == VRPlayerState.Solid && !_StopMovement;
    public bool CanConsume() => PlayerState == VRPlayerState.Voxel && !_StopMovement;
    public bool CanCollapse() => PlayerState == VRPlayerState.Solid && !_StopMovement;

    protected void SetCharacterState(bool state)
    {
        _CharacterObj.SetActive(state);
        _Collider.enabled = state;
    }

    #endregion
}

