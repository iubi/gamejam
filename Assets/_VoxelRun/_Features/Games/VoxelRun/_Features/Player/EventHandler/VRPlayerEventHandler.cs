﻿using UnityEngine;

public class VRPlayerEventHandler : MonoBehaviour
{
    [SerializeField] private VRBaseCharacterController _playerController;

    public void OnCollisionEnter(Collision collision)
    {
        PlayerHit(collision.gameObject);
    }

    public void OnTriggerEnter(Collider other)
    {
        PlayerHit(other.gameObject);
    }

    private void PlayerHit(GameObject obj)
    {
        //if(obj.CompareTag("Hurdle"))
        //{
        //    _playerController.CollidedWithHurdle();
        //}
        //else if (obj.CompareTag("Collectables") && _playerController.CanCollect())
        //{
        //    //var collectable = obj.GetComponent<VRCollectables>();
        //    //collectable.Collect(_playerController.PlayerPredictedPosition, _playerController.IsNpc);
        //    //_playerController.CollectVoxels(collectable.Strength);
        //}
        //else if (obj.CompareTag("Npc") && _playerController.CanConsume())
        //{
        //    var npc = obj.GetComponentInParent<VRBaseCharacterController>();
        //    if (npc.CanCollapse())
        //    {
        //        //var voxels = npc.Collapse(_playerController.PlayerBoostSpeed);
        //        //_playerController.ConsumeOpponent(voxels);
        //    }
        //}
    }
}
