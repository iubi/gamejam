using MSKit.Events;

public static partial class EventManager
{
    public static Observable<EvalPoint> OnPlayerPointUpdate = new Observable<EvalPoint>(ObserverType.Parameter);
    public static void DoFirePlayerPointUpdateEvent(EvalPoint point) => OnPlayerPointUpdate.Fire(point);

    public static Observable<float> OnPlayerScaleUpdate = new Observable<float>(ObserverType.Parameter);
    public static void DoFirePlayerScaleUpdateEvent(float scale) => OnPlayerScaleUpdate.Fire(scale);

    public static Observable<VRPlayerState> OnPlayerStateChange = new Observable<VRPlayerState>(ObserverType.Parameter);
    public static void DoFirePlayerStateChangeEvent(VRPlayerState state) => OnPlayerStateChange.Fire(state);

    public static Observable<PlayerRacePositionModel> OnGetPlayerRacePositionModel = new Observable<PlayerRacePositionModel>(ObserverType.Callback);
    public static PlayerRacePositionModel DoFireGetPlayerRacePositionModelEvent() => OnGetPlayerRacePositionModel.Fire();

    public static Observable<int> OnGetPlayerFinalRacePosition = new Observable<int>(ObserverType.Callback);
    public static int DoFireGetPlayerFinalRacePosition() => OnGetPlayerFinalRacePosition.Fire();

    public static Observable<PlayerRacePositionModel> OnSetPlayerRacePositionModel = new Observable<PlayerRacePositionModel>(ObserverType.Parameter);
    public static void DoFireSetPlayerRacePositionModelEvent(PlayerRacePositionModel model) => OnSetPlayerRacePositionModel.Fire(model);

    public static Observable<int> OnSetPlayerRacePosition = new Observable<int>(ObserverType.Parameter);
    public static void DoFireSetPlayerRacePositionEvent(int position) => OnSetPlayerRacePosition.Fire(position);

    public static Observable<float> OnGetPlayerInterpolationValue = new Observable<float>(ObserverType.Callback);
    public static float DoFireGetPlayerInterpolationValueEvent() => OnGetPlayerInterpolationValue.Fire();
}

public struct PlayerRacePositionModel
{
    public int PlayerId;
    public float InterpolationValue;
    public int PlayerRacePosition;
}