﻿using UnityEngine;

public partial class GameConfig : ScriptableObject
{
    [Header("Jet Pack")]
    public float JetPackParticlesLengthMultiplier;
    public float JetPackMinFuelToAnimate;
    public Color JetPackParticlesStartColor;
    public Color JetPackParticlesEndColor;
}
