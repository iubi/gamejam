﻿//using MSKit.Configurations;
using UnityEngine;
using DG.Tweening;
//using MoreMountains.NiceVibrations;
//using MSKit.Database;

public class JetPackController : MonoBehaviour
{
    [SerializeField] GameConfig _configs;
    [SerializeField] MeshRenderer _jetpackRenderer;
    [SerializeField] ParticleSystem _jetParticles;
    [SerializeField] Transform _particlesTransform;

    private MaterialPropertyBlock _materialPropertyBlock;
    private int _dissolveId;
    private int _noiseId;

    private Vector3 _particlesInitialScale;

    private float _currentNoise = 50;

    private bool _particlesHidden;

    private Tween _noiseTween;

    public float Fuel { get; private set; }

    public void Initialize()
    {
        _dissolveId = Shader.PropertyToID("_Dissolve");
        _noiseId = Shader.PropertyToID("_NoiseScale");
        _materialPropertyBlock = new MaterialPropertyBlock();
        _jetpackRenderer.GetPropertyBlock(_materialPropertyBlock);

        _particlesInitialScale = _particlesTransform.localScale;
    }

    public void SetFuel(float fuel)
    {
        Fuel = fuel;
        ClampFuel();
        UpdateJetPackState();
    }

    public void AddFuel(float fuelToAdd)
    {
        Fuel += fuelToAdd;
        ClampFuel();
        UpdateJetPackState();
    }

    public bool ConsumeFuel(bool animate)
    {
        var fuelToConsume = Time.fixedDeltaTime * _configs.PlayerDeteriorationRate;

        Fuel -= fuelToConsume;
        ClampFuel();
        UpdateJetPackState();

        if(animate)
        {
            if (Fuel < _configs.JetPackMinFuelToAnimate && !_particlesHidden)
            {
                _particlesHidden = true;
                HideParticles();
            }
            else if (_particlesHidden)
            {
                _particlesHidden = false;
                PlayParticles();
            }

            UpdateParticlesColor();

            //if (HapticState.GetHapticState())
            //{
            //    var vibrationValue = Mathf.Lerp(0.25f, 0.15f, Fuel);
            //    MMVibrationManager.UpdateContinuousHaptic(vibrationValue, vibrationValue, true);
            //}
        }

        var scale = _particlesInitialScale;
        scale.z = 0.25f + Fuel * _configs.JetPackParticlesLengthMultiplier;
        _particlesTransform.localScale = scale;

        return Fuel <= 0;
    }

    public void RandomizeNoise()
    {
        var targetNoise = _currentNoise < 55f ? 150f : 50f;
        _noiseTween.Kill();
        _noiseTween = DOTween.To(() => _currentNoise, x => _currentNoise = x, targetNoise, 10f)
            .OnUpdate(() =>
            {
                _materialPropertyBlock.SetFloat(_noiseId, _currentNoise);
                _jetpackRenderer.SetPropertyBlock(_materialPropertyBlock);
            })
            .OnComplete(RandomizeNoise);
    }

    public void StopNoise()
    {
        _noiseTween.Kill();
    }

    public void PlayParticles()
    {
        _jetParticles.Play();
    }

    public void HideParticles()
    {
        _jetParticles.Stop();
        _jetParticles.Clear();
    }

    private void UpdateJetPackState()
    {
        _materialPropertyBlock.SetFloat(_dissolveId, Fuel);
        _jetpackRenderer.SetPropertyBlock(_materialPropertyBlock);
    }

    private void UpdateParticlesColor()
    {
        var startColor = _configs.JetPackParticlesStartColor;
        var endColor = _configs.JetPackParticlesEndColor;

        var color = Color.Lerp(startColor, endColor, Fuel);
        var main = _jetParticles.main;
        main.startColor = color;
    }

    private void ClampFuel() => Fuel = Mathf.Clamp(Fuel, 0, 1);
}
