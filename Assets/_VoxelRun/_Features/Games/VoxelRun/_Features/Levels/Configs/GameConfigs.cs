using UnityEngine;

public partial class GameConfig : ScriptableObject
{
    [Header("Levels")]
    public float[] PlatformLengths;
}
