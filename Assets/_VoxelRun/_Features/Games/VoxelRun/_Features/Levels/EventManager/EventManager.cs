using MSKit.Events;

public static partial class EventManager
{
    public static Observable<VRPlatformData> OnAddPlatformData = new Observable<VRPlatformData>(ObserverType.Parameter);
    public static void DoFireAddLevelsDataEvent(VRPlatformData data) => OnAddPlatformData.Fire(data);

    public static Observable<VRPlatformData> OnGetCurrentPlatformData = new Observable<VRPlatformData>(ObserverType.Callback);
    public static VRPlatformData DoFireGetCurrentPlatformDataEvent() => OnGetCurrentPlatformData.Fire();

    public static Observable<float> OnGetPlatformLength = new Observable<float>(ObserverType.Callback);
    public static float DoFireGetPlatformLengthEvent() => OnGetPlatformLength.Fire();
}