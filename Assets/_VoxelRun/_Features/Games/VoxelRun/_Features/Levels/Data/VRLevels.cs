﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct VRPlatformData
{
    public List<Vector2> Path;
}