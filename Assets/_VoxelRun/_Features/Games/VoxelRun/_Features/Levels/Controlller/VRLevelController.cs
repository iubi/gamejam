﻿public class VRLevelController
{
    private VRPlatformData _platformData;
    private float _platformLength;

    public VRLevelController()
    {
        RegisterEvents();
        //VRLevelParser.ParseLevelJson();
    }

    private void RegisterEvents()
    {
        EventManager.OnAddPlatformData += SetPlatformData;
        EventManager.OnGetPlatformLength += GetPlatformLength;
        EventManager.OnGetCurrentPlatformData += GetPlatformData;
    }

    private float GetPlatformLength() => _platformLength;
    private VRPlatformData GetPlatformData() => _platformData;
    private void SetPlatformData(VRPlatformData data)
    {
        _platformData = data;
        _platformLength = _platformData.Path.ConvertToVector3().GetTotalLength();
    }
}
