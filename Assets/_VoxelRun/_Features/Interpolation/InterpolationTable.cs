using System.Collections.Generic;
using UnityEngine;

public class InterpolationTable
{
    private float _length;

    private readonly List<float> _distances = new List<float>();
    private readonly List<float> _cumulativeDistances = new List<float>();
    private readonly List<float> _turnsTime = new List<float>();
    private readonly List<Vector3> _points = new List<Vector3>();
    private readonly List<Vector3> _tangents = new List<Vector3>();

    public List<Vector3> Points => _points;
    public float Length => _length;

    private void Reset()
    {
        _distances.Clear();
        _cumulativeDistances.Clear();
        _turnsTime.Clear();
        _points.Clear();
        _tangents.Clear();
    }

    public void InitializePoints(List<Vector3> points)
    {
        Reset();

        _points.AddRange(points);

        _distances.Add(0);
        _cumulativeDistances.Add(0);

        var length = 0f;
        for (var i = 1; i < points.Count; i++)
        {
            var p0 = points[i - 1];
            var p1 = points[i];

            _tangents.Add((p1 - p0).normalized);

            var dist = (p0 - p1).magnitude;
            _distances.Add(dist);

            length += dist;
            _cumulativeDistances.Add(length);
        }

        _tangents.Add((points[points.Count - 1] - points[points.Count - 1]).normalized);

        _length = length;
    }

    public EvalPoint Evaluate(float t)
    {
        var l = t * _length;

        var idx = 0;
        for (; idx < _cumulativeDistances.Count; idx++)
            if (_cumulativeDistances[idx] > l)
                break;

        if (idx <= 0)
            return new EvalPoint(_points[0], _tangents[0]);

        var idxMax = _points.Count - 1;
        if (idx > idxMax)
            return new EvalPoint(_points[idxMax], _tangents[idxMax]);

        var s = (l - _cumulativeDistances[idx - 1]) / _distances[idx];

        var point = (1 - s) * _points[idx - 1] + s * _points[idx];
        var tangent = _tangents[idx-1];
//        //Smuf tangent
//        var tangent = (1 - s) * _tangents[idx - 1] + s * _tangents[idx];
        return new EvalPoint(point, tangent);
    }

    public EvalPoint Evaluate(Vector3 point, out float t)
    {
        var idx = GetClosestIndex(point, out var s);

        var idxMax = _points.Count - 1;
        if (idx >= idxMax)
        {
            t = 1;
            return new EvalPoint(_points[idxMax], _tangents[idxMax]);
        }

        t = (s * (idx >= _points.Count - 1 ? 0 : _distances[idx + 1]) + _cumulativeDistances[idx]) / _length;
        return new EvalPoint(
            (1 - s) * _points[idx] + s * _points[idx + 1],
            (1 - s) * _tangents[idx] + s * _tangents[idx + 1]);
    }

    public EvalPoint Evaluate(Vector3 point)
    {
        var idx = GetClosestIndex(point, out var s);
        var idxMax = _points.Count - 1;
        return idx >= idxMax
            ? new EvalPoint(_points[idxMax], _tangents[idxMax])
            : new EvalPoint(
                (1 - s) * _points[idx] + s * _points[idx + 1],
                (1 - s) * _tangents[idx] + s * _tangents[idx + 1]);
    }

    public float GetEvaluation(Vector3 point)
    {
        var idx = GetClosestIndex(point, out var s);
        var segmentDist = idx >= _points.Count - 1 ? 0 : _distances[idx + 1];
        return (s * segmentDist + _cumulativeDistances[idx]) / _length;
    }

    public bool IsPointAwayFromTurn(float time, float awayDelta)
    {
        for (int i = 0; i < _turnsTime.Count; i += 2)
        {
            var turnTimeMin = _turnsTime[i] - awayDelta;
            var turnTimeMax = _turnsTime[i + 1] + awayDelta;

            if (time > turnTimeMin && time < turnTimeMax)
                return false;
        }

        return true;
    }

    public void CalculateTurns()
    {
        _turnsTime.Clear();

        for (int i = 1; i < _points.Count - 1; i++)
        {
            var time = GetEvaluation(_points[i]);
            _turnsTime.Add(time);
        }
    }

    private int GetClosestIndex(Vector3 point, out float s)
    {
        var min = float.MaxValue;
        var idx = 0;
        s = 0f;

        for (var i = 0; i < _points.Count - 1; i++)
        {
            var p0 = _points[i];
            var p1 = _points[i + 1];
            var p10 = p1 - p0;
            var tOnSegment = Vector3.Dot(point - p0, p10) / Vector3.Dot(p10, p10);
            if (tOnSegment >= 0 && tOnSegment <= 1)
            {
                var pointOnSegment = p0 + tOnSegment * p10;
                var pointSegmentSqrDist = (point - pointOnSegment).sqrMagnitude;
                if (pointSegmentSqrDist < min)
                {
                    min = pointSegmentSqrDist;
                    s = tOnSegment;
                    idx = i;
                }
            }

            var dist = (point - p0).sqrMagnitude;
            if (dist < min)
            {
                min = dist;
                idx = i;
            }
        }

        var distFinal = (point - _points[_points.Count - 1]).sqrMagnitude;

        return distFinal < min ? _points.Count - 1 : idx;
    }
}
