using UnityEngine;

public struct EvalPoint
{
    public Vector3 Point { get; set; }
    public Vector3 Tangent { get; }

    public EvalPoint(Vector3 point, Vector3 tangent)
    {
        Point = point;
        Tangent = tangent;
    }
}