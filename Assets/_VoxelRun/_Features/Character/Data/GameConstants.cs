﻿using UnityEngine;

public static partial class GameConstants
{
    public static readonly int MovementTypeKey = Animator.StringToHash("MovementType");
    public static readonly int DeathTypeKey = Animator.StringToHash("DeathType");
    public static readonly int IdleTriggerKey = Animator.StringToHash("Idle");
    public static readonly int DeathTriggerKey = Animator.StringToHash("Death");
    public static readonly int DiveStartTriggerKey = Animator.StringToHash("DiveStart");
    public static readonly int DiveEndTriggerKey = Animator.StringToHash("DiveEnd");
    public static readonly int FallTriggerKey = Animator.StringToHash("Fall");
    public static readonly int RunTriggerKey = Animator.StringToHash("Run");
}
