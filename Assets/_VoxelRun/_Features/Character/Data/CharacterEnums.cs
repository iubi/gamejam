﻿public enum CharacterAnimationType
{
    None = -1,
    Idle = 0,
    Run = 1,
    Death = 2,
    Victory = 3,
    DiveStart = 4,
    DiveEnd = 5,
    Fall = 6,
}
