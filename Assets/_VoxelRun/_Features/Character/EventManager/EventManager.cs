﻿using MSKit.Events;

public static partial class EventManager
{
    public static Observable OnCharacterEnterRunState = new Observable(ObserverType.Default);
    public static void DoFireCharacterEnterRunStateEvent() => OnCharacterEnterRunState.Fire();

    public static Observable OnCharacterExitRunState = new Observable(ObserverType.Default);
    public static void DoFireCharacterExitRunStateEvent() => OnCharacterExitRunState.Fire();
}
