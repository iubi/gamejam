﻿using UnityEngine;

public class PlayerRunAnimationState : StateMachineBehaviour
{
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        EventManager.DoFireCharacterEnterRunStateEvent();
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        EventManager.DoFireCharacterExitRunStateEvent();
    }
}
