﻿using UnityEngine;

public class CharacterAnimationController
{
    private Animator _characterAnimator;
    private CharacterAnimationType _animationType = CharacterAnimationType.None;

    public CharacterAnimationController(Animator animator) => _characterAnimator = animator;

    public void PlayAnimation(CharacterAnimationType animationType)
    {
        if (_animationType == animationType) return;

        _animationType = animationType;
        
        switch (_animationType)
        {
            case CharacterAnimationType.Idle:
                PlayIdle();
                break;
            case CharacterAnimationType.Run:
                PlayRun();
                break;
            case CharacterAnimationType.Death:
                PlayDeath();
                break;
            case CharacterAnimationType.Victory:
                PlayVictory();
                break;
            case CharacterAnimationType.DiveStart:
                PlayDiveStart();
                break;
            case CharacterAnimationType.DiveEnd:
                PlayDiveEnd();
                break;
            case CharacterAnimationType.Fall:
                PlayFallDown();
                break;
        }
    }

    private void PlayIdle()
    {
        SetAnimatorState(GameConstants.MovementTypeKey, (int)CharacterAnimationType.Idle);
        TriggerAnimation(GameConstants.IdleTriggerKey);
    }

    private void PlayRun()
    {
        SetAnimatorState(GameConstants.MovementTypeKey, (int)CharacterAnimationType.Run);
        TriggerAnimation(GameConstants.RunTriggerKey);
    }

    private void PlayDeath()
    {
        var deathType = Random.Range(1, 3);
        SetAnimatorState(GameConstants.DeathTypeKey, deathType);
        TriggerAnimation(GameConstants.DeathTriggerKey);
    }

    private void PlayVictory()
    {
        SetAnimatorState(GameConstants.MovementTypeKey, (int)CharacterAnimationType.Victory);
    }

    private void PlayDiveStart()
    {
        SetAnimatorState(GameConstants.MovementTypeKey, (int)CharacterAnimationType.DiveStart);
        TriggerAnimation(GameConstants.DiveStartTriggerKey);
    }

    private void PlayDiveEnd()
    {
        SetAnimatorState(GameConstants.MovementTypeKey, (int)CharacterAnimationType.Run);
        TriggerAnimation(GameConstants.DiveEndTriggerKey);
    }

    private void PlayFallDown()
    {
        SetAnimatorState(GameConstants.MovementTypeKey, (int)CharacterAnimationType.Run);
        TriggerAnimation(GameConstants.FallTriggerKey);
    }

    public void SetAnimatorSpeed(float speed) => _characterAnimator.speed = speed;
    private void SetAnimatorState(int id, int state) => _characterAnimator.SetInteger(id, state);
    private void TriggerAnimation(int id) => _characterAnimator.SetTrigger(id);
}
