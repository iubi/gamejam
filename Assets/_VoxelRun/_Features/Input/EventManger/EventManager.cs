﻿using MSKit.Events;
using UnityEngine;

public static partial class EventManager
{
	public static Observable<Vector2> OnDragStart = new Observable<Vector2>(ObserverType.Parameter);
	public static void DoFireDragStartEvent(Vector2 inputPosition) => OnDragStart.Fire(inputPosition);

	public static Observable<Vector2> OnDragEnd = new Observable<Vector2>(ObserverType.Parameter);
	public static void DoFireDragEndEvent(Vector2 inputPosition) => OnDragEnd.Fire(inputPosition);
	
	public static Observable<Vector3> OnInputDrag = new Observable<Vector3>(ObserverType.Parameter);
	public static void DoFireInputDragEvent(Vector2 inputPosition) => OnInputDrag.Fire(inputPosition);

	public static Observable OnActivateInput = new Observable(ObserverType.Default);
	public static void DoFireActivateInputEvent() => OnActivateInput.Fire();

	public static Observable OnDeactivateInput = new Observable(ObserverType.Default);
	public static void DoFireDeactivateInputEvent() => OnDeactivateInput.Fire();

	public static Observable OnFingerDown = new Observable(ObserverType.Default);
	public static void DoFireFingerDownEvent() => OnFingerDown.Fire();

	public static Observable OnFingerUp = new Observable(ObserverType.Default);
	public static void DoFireFingerUpEvent() => OnFingerUp.Fire();

	public static Observable<InputSwipeDirection> OnInputSwipe = new Observable<InputSwipeDirection>(ObserverType.Parameter);
	public static void DoFireInputSwipeEvent(InputSwipeDirection inputSwipeDirection) => OnInputSwipe.Fire(inputSwipeDirection);
}
