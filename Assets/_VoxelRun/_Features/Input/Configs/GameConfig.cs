﻿using UnityEngine;

public partial class GameConfig : ScriptableObject
{
    [Header("Input")]
    public float InputDragSpeed = 0.5f;
    public float InputSwipeThreshold = 1f;
}
