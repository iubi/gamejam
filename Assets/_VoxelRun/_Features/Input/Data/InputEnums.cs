﻿public enum InputSwipeDirection
{
    None,
    Right,
    Left,
    Up,
    Down
}