﻿using UnityEngine;

public class InputDragController
{
    public void Drag(Vector2 position)
    {
        EventManager.DoFireInputDragEvent(position);
    }
    
    public void DragEnd(Vector2 position)
    {
        EventManager.DoFireDragEndEvent(position);
    }

    public void DragStart(Vector2 position)
    {
        EventManager.DoFireDragStartEvent(position);
    }
}
