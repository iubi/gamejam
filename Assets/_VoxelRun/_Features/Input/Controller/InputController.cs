﻿using UnityEngine;
using UnityEngine.EventSystems;

public class InputController : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField] float inputSwipeThreshold;
    [SerializeField] private GameObject InputObject;

    private InputSwipeController _inputSwipeController;
    private readonly InputDragController _inputDragController = new InputDragController();

    private void Awake()
    {
        _inputSwipeController = new InputSwipeController(inputSwipeThreshold);

        InitializeEvents();
    }

    private void InitializeEvents()
    {
        EventManager.OnActivateInput += ActivateInput;
        EventManager.OnDeactivateInput += DeActivateInput;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        EventManager.DoFireFingerUpEvent();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        EventManager.DoFireFingerDownEvent();
    }

    public void OnDrag(PointerEventData eventData)
    {
        _inputDragController.Drag(eventData.delta);
    }
    public void OnBeginDrag(PointerEventData eventData)
    {
        _inputDragController.DragStart(eventData.position);
        _inputSwipeController.SwipeBegin(eventData.position);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        _inputDragController.DragEnd(eventData.position);
        _inputSwipeController.SwipeEnd(eventData.position);
    }
    
    private void ActivateInput()
    {
        _inputSwipeController.Reset();
        SetInputState(true);
    }

    private void DeActivateInput() => SetInputState(false);
    private void SetInputState(bool state) => InputObject.SetActive(state);
}
