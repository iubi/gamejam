﻿//using MSKit.Configurations;
using UnityEngine;

public class InputSwipeController
{
    private Vector2 _dragStartPosition;
    private Vector2 _dragEndPosition;
    private float _swipeThreshold;

    public InputSwipeController(float swipeThreshold)
    {
        _swipeThreshold = swipeThreshold;
    }

    public void SwipeBegin(Vector2 position) => _dragStartPosition = position;

    public void SwipeEnd(Vector2 position)
    {
        _dragEndPosition = position;
        CheckForSwipe();
    }

    public void Reset() => _dragStartPosition = _dragEndPosition = Vector2.zero;

    private void CheckForSwipe()
    {
        //var swipeThreshold = Configs.GameConfigs.InputSwipeThreshold;
        var verticalDelta = Mathf.Abs(_dragEndPosition.y - _dragStartPosition.y);
        var horizontalDelta = Mathf.Abs(_dragEndPosition.x - _dragStartPosition.x);

        var swipeDirection = InputSwipeDirection.None;

        if (verticalDelta > _swipeThreshold && verticalDelta > horizontalDelta)
        {
            var swipeDelta = _dragEndPosition.y - _dragStartPosition.y;
            swipeDirection = swipeDelta > 0 ? InputSwipeDirection.Up : InputSwipeDirection.Down;
        }
        else if (horizontalDelta > _swipeThreshold && horizontalDelta > verticalDelta)
        {
            var swipeDelta = _dragEndPosition.x - _dragStartPosition.x;
            swipeDirection = swipeDelta > 0 ? InputSwipeDirection.Right : InputSwipeDirection.Left;
        }

        EventManager.DoFireInputSwipeEvent(swipeDirection);
    }
}
