using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GameConfig _configs;
    [SerializeField] private VRPlatformController _platformController;
    [SerializeField] private VRPlayerController _playerController;
    [SerializeField] private VRCameraController _cameraController;
    [SerializeField] private VRNpcController _npcController;

    private VRLevelController _levelController;

    private void Awake()
    {
        Initialize();
    }

    private void Start()
    {
        LoadGame();
        StartGame();
    }

    private void Initialize()
    {
        _levelController = new VRLevelController();

        _platformController.Initialize();
        _playerController.Initialize();
        _cameraController.Initialize();
        _npcController.Initialize();
    }

    private void LoadGame()
    {
        _platformController.LoadGame(true, _configs.PlatformLengths[1]);
        _playerController.LoadGame(true);
        _cameraController.LoadGame();
        _npcController.LoadGame(true, _configs.NpcToSpawn);
    }

    private void StartGame()
    {
        _playerController.StartGame();
        _cameraController.StartGame();
        _npcController.StartGame();
    }

    private void UnloadGame()
    {
        _playerController.UnloadGame();
        _platformController.UnloadGame();
        _npcController.UnloadGame();
    }

    private void CompleteGame()
    {
        _cameraController.CompleteGame();
    }

    private void FailGame()
    {
        _cameraController.FailGame();
    }
}
