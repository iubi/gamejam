﻿public enum ObserverType
{
    Default = 0,
    Callback,
    Parameter,
    CallbackAndParameter
}