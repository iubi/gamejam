﻿using System;
using System.Collections.Generic;

namespace MSKit.Events
{
    public class Observable
    {
        private readonly HashSet<Action> _actions = new HashSet<Action>();

        protected ObserverType _ObserverType;

        public Observable(ObserverType type) => _ObserverType = type;

        private void AddObserver(Action action) => _actions.Add(action);
        private void RemoveObserver(Action action) => _actions.Remove(action);

        public void Fire()
        {
            foreach (var action in _actions)
            {
                action.Invoke();
            }
        }

        public static Observable operator +(Observable events, Action action)
        {
            events.AddObserver(action);
            return events;
        }

        public static Observable operator -(Observable events, Action action)
        {
            events.RemoveObserver(action);
            return events;
        }

        protected bool Validate(ObserverType type)
        {
            var isValidCall = _ObserverType == type;

            if (!isValidCall)
                LogWarning(type);

            return isValidCall;
        }

        private void LogWarning(ObserverType type) => UnityEngine.Debug.LogWarning( string.Format("<color=red>ERROR: </color> Registering/Un-Registering Invalid Observer of type '{0}', type must be '{1}'", type, _ObserverType));
    }

    public class Observable<T> : Observable where T : struct
    {
        private readonly HashSet<Func<T>> _functions = new HashSet<Func<T>>();
        private readonly HashSet<Action<T>> _actions = new HashSet<Action<T>>();

        public Observable(ObserverType type) : base(type) { }

        private void AddObserver(Action<T> action) => _actions.Add(action);
        private void RemoveObserver(Action<T> action) => _actions.Remove(action);
        private void AddObserver(Func<T> action) => _functions.Add(action);
        private void RemoveObserver(Func<T> action) => _functions.Remove(action);

        public void Fire(T param)
        {
            foreach (var action in _actions)
            {
                action.Invoke(param);
            }
        }

        public new T Fire()
        {
            foreach (var action in _functions)
            {
                return action.Invoke();
            }

            return default;
        }

        public static Observable<T> operator +(Observable<T> events, Action<T> action)
        {
            if (events.Validate(ObserverType.Parameter))
                events.AddObserver(action);
            return events;
        }

        public static Observable<T> operator -(Observable<T> events, Action<T> action)
        {
            if (events.Validate(ObserverType.Parameter))
                events.RemoveObserver(action);
            return events;
        }

        public static Observable<T> operator +(Observable<T> events, Func<T> action)
        {
            if (events.Validate(ObserverType.Callback))
                events.AddObserver(action);
            return events;
        }

        public static Observable<T> operator -(Observable<T> events, Func<T> action)
        {
            if (events.Validate(ObserverType.Callback))
                events.RemoveObserver(action);
            return events;
        }
    }

    public class Observable<ParameterType, ReturnType> : Observable where ParameterType : struct where ReturnType : struct
    {
        private readonly HashSet<Func<ParameterType, ReturnType>> _actions = new HashSet<Func<ParameterType, ReturnType>>();

        public Observable(ObserverType type) : base(type) { }

        private void AddObserver(Func<ParameterType, ReturnType> action) => _actions.Add(action);

        private void RemoveObserver(Func<ParameterType, ReturnType> action) => _actions.Remove(action);

        public ReturnType Fire(ParameterType param)
        {
            foreach (var action in _actions)
            {
                return action.Invoke(param);
            }

            return default;
        }

        public static Observable<ParameterType, ReturnType> operator +(Observable<ParameterType, ReturnType> events, Func<ParameterType, ReturnType> action)
        {
            events.AddObserver(action);
            return events;
        }

        public static Observable<ParameterType, ReturnType> operator -(Observable<ParameterType, ReturnType> events, Func<ParameterType, ReturnType> action)
        {
            events.RemoveObserver(action);
            return events;
        }
    }
}