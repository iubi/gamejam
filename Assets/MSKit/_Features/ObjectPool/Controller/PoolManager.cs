﻿using System.Collections.Generic;
using UnityEngine;

namespace MSKit.ResourcePool
{
    public class PoolManager : MonoBehaviour
    {
        [SerializeField] protected Transform _PoolContainer;
        private static Transform _poolContainer;
        private readonly static Dictionary<Pool, Pool<Component>> _poolObjects = new Dictionary<Pool, Pool<Component>>();

        private void Awake() => _poolContainer = _PoolContainer;

        public static void CreatePool<T>(Pool key, T obj, int initialSize = 1, int maxSize = 10) where T : Component
        {
            ExtendPool(key, obj, initialSize, maxSize);
        }

        public static void AddToPool<T>(Pool key, T objectToPool) where T : Component
        {
            ExtendPool(key, objectToPool);
            _poolObjects[key].Add(objectToPool);
        }

        public static T GetFromPool<T>(Pool key, T objectToFind) where T : Component
        {
            ExtendPool(key, objectToFind);
            T objectToReturn = (T)_poolObjects[key].Get();
            return objectToReturn;
        }

        public static void ResetPool<T>(Pool key) where T : Component
        {
            if (!_poolObjects.ContainsKey(key)) return;

            _poolObjects[key].Reset();
        }

        public static void DestroyPool<T>(Pool key) where T : Component
        {
            if (!_poolObjects.ContainsKey(key)) return;

            _poolObjects[key].Destroy();
            _poolObjects.Remove(key);
        }

        private static void ExtendPool<T>(Pool key, T value, int initialSize = 0, int maxSize = 10) where T : Component
        {
            if (_poolObjects.ContainsKey(key)) return;

            var pool = new Pool<Component>(_poolContainer, value, initialSize, maxSize);
            _poolObjects.Add(key, pool);
        }
    }
}