﻿using System.Collections.Generic;
using UnityEngine;

namespace MSKit.ResourcePool
{
    public class Pool<T> where T : Component
    {
        private Transform _poolContainer;
        private readonly Queue<T> _poolObjects = new Queue<T>();
        private readonly HashSet<T> _activeObjects = new HashSet<T>();

        private T _objectToPool;

        private int _maxSize;

        public Pool(Transform parent, T objectToPool, int initialSize = 0, int maxSize = 10)
        {
            var container = new GameObject
            {
                name = objectToPool.name
            };

            _maxSize = maxSize;

            _poolContainer = container.transform;
            _poolContainer.SetParent(parent);
            _objectToPool = objectToPool;
            _poolContainer.localPosition = Vector3.zero;
            _poolContainer.localRotation = Quaternion.identity;

            Populate(initialSize);
        }

        public T Get()
        {
            if (_poolObjects.Count == 0)
                Extend(_objectToPool);

            T objectToReturn = _poolObjects.Dequeue();
            _activeObjects.Add(objectToReturn);
            return objectToReturn;
        }

        public void Reset()
        {
            foreach (var obj in _activeObjects)
                Insert(obj);

            _activeObjects.Clear();
            RemoveOverloadedPool();
        }

        public void Add(T obj)
        {
            _activeObjects.Remove(obj);

            Insert(obj);
            RemoveOverloadedPool();
        }

        public void Destroy()
        {
            Reset();
            _poolObjects.Clear();
            Object.Destroy(_poolContainer.gameObject);
        }

        private void Populate(int size)
        {
            if (size > _maxSize) size = _maxSize;

            for (int i = 0; i < size; i++)
            {
                Extend(_objectToPool);
            }
        }

        private void Insert(T objectToPool)
        {
            var obj = objectToPool.gameObject;
            obj.SetActive(false);
            obj.transform.SetParent(_poolContainer);
            _poolObjects.Enqueue(objectToPool);
        }

        private void Extend(T objectToPool)
        {
            var obj = Spawn(objectToPool);
            Insert(obj);
        }

        private void RemoveOverloadedPool()
        {
            var objectsToRemove = _poolObjects.Count - _maxSize;

            if (objectsToRemove <= 0) return;

            for (int i = 0; i < objectsToRemove; i++)
            {
                var obj = _poolObjects.Dequeue();
                Object.Destroy(obj.gameObject);
            }
        }

        private T Spawn(T objectToPool) => Object.Instantiate(objectToPool, _poolContainer);
    }
}