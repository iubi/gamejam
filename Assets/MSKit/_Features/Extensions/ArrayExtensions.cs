using UnityEngine;

public static class ArrayExtensions
{
    public static T GetAtIndexClamped<T>(this T[] array, int index)
    {
        return array.Length == 0
            ? default
            : array[Mathf.Clamp(index, 0, array.Length - 1)];
    }
    
    public static T GetAtIndexWrapped<T>(this T[] array, int index)
    {
        return array.Length == 0
            ? default
            : array[index % array.Length];
    }

    public static T[] Shuffle<T>(this T[] array)
    {
        for (int i = 0; i < array.Length; i++)
        {
            var rnd = Random.Range(0, array.Length);
            var tempGO = array[rnd];
            array[rnd] = array[i];
            array[i] = tempGO;
        }

        return array;
    }
}
