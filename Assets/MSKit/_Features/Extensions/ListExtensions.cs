﻿using System.Collections.Generic;
using UnityEngine;

public static class ListExtensions
{
    public static void Shuffle<T>(this IList<T> ts)
    {
        var count = ts.Count;
        var last = count - 1;
        for (var i = 0; i < last; ++i)
        {
            var r = UnityEngine.Random.Range(i, count);
            var tmp = ts[i];
            ts[i] = ts[r];
            ts[r] = tmp;
        }
    }

    public static void RemoveLast<T>(this IList<T> ts)
    {
        var count = ts.Count;
        var last = count - 1;
        ts.RemoveAt(last);
    }

    public static T GetAndRemoveLast<T>(this IList<T> ts)
    {
        var count = ts.Count;
        var last = count - 1;
        var item = ts[last];

        ts.RemoveAt(last);

        return item;
    }

    public static T Last<T>(this IList<T> ts)
    {
        var last = ts.Count - 1;
        return ts[last];
    }

    public static List<Vector3> ConvertToVector3(this List<Vector2> ts)
    {
        var v3List = new List<Vector3>();
        var last = ts.Count;

        for (int i = 0; i < last; i++)
        {
            var v3 = ts[i].XZ();
            v3List.Add(v3);
        }

        return v3List;
    }

    public static List<Vector2> ConvertToVector2(this List<Vector3> ts)
    {
        var v2List = new List<Vector2>();
        var last = ts.Count;

        for (int i = 0; i < last; i++)
        {
            var v2 = ts[i].XZ();
            v2List.Add(v2);
        }

        return v2List;
    }
}
