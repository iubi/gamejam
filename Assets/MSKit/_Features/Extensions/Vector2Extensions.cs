using UnityEngine;

public static class Vector2Extensions
{
    public static Vector3 XZ(this Vector2 v) => new Vector3(v.x, 0, v.y);

    public static int GetIndexOfClosestPoint(this Vector2[] collection, Vector2 point)
    {
        var pos = new Vector2(point.x, point.y);
        var closestIndex = 0;
        var closestDistance = float.MaxValue;
        
        for (var i = 0; i < collection.Length; ++i)
        {
            var dist = (pos - collection[i]).sqrMagnitude;
            if (dist < closestDistance)
            {
                closestDistance = dist;
                closestIndex = i;
            }
        }

        return closestIndex;
    }
}
