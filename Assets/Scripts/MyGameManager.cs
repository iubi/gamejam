using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.SceneManagement;
public class MyGameManager : MonoBehaviour
{
    public static MyGameManager instance;
    public Animator playerAnimator;
    public RuntimeAnimatorController playerCombatAnimator;
    public bool gameStarted = false;
    public CinemachineVirtualCamera playerCamera;
    public int winningQuota;
    public GameObject attackBTN, gameOverPanel, gameWonPanel;
    public AudioSource audioSource;
    public AudioClip armorUp, armorDown;

    public GameObject playerFollowCamera, playerFightCamera;
    public GameObject sword;
    // Start is called before the first frame update
    void Start()
    {
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void StartRunning()
    {
        playerAnimator.SetBool("GameStarted", true);
        gameStarted = true;
    }

    public void AssignPlayerToCineMachineCameraLookAt()
    {
        playerCamera.LookAt = Armor.instance.transform;
    }

    public void PlayerVictory()
    {
        if (sword.activeInHierarchy) sword.SetActive(false);
        playerAnimator.SetBool("Victory", true);
        Invoke("ActivateGameWonPanel", 3f);

    }

    public void PlayerDead()
    {
        playerAnimator.SetBool("isDead", true);
        Invoke("ActivateGameOverPanel", 3f);
    }

    private void ActivateGameOverPanel()
    {
        gameOverPanel.SetActive(true);
        attackBTN.SetActive(false);
    }

    private void ActivateGameWonPanel()
    {
        gameWonPanel.SetActive(true);
        attackBTN.SetActive(false);
    }

    public void Retry()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void NextLevel(int _levelIndex)
    {
        SceneManager.LoadScene(_levelIndex);
    }

    public void ArmorUp()
    {
        if (!audioSource.isPlaying)
            audioSource.PlayOneShot(armorUp);
    }
    public void ArmorDown()
    {
        if (!audioSource.isPlaying)
            audioSource.PlayOneShot(armorDown);
    }

    public void DisablePlayerFollowCamera()
    {
        gameStarted = false;
        playerFightCamera.SetActive(true);
        playerFollowCamera.SetActive(false);
    }
}
