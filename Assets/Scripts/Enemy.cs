using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    public static Enemy instance;
    public Animator animator;
    public bool isPlayerReached = false;
    public int health = 7;
    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isPlayerReached)
        {
            transform.LookAt(Armor.instance.transform);
            transform.position = Vector3.Lerp(transform.position, new Vector3(Armor.instance.transform.position.x + .5f, Armor.instance.transform.position.y, Armor.instance.transform.position.z + 1.5f), Time.deltaTime);
        }
    }


    public void PlayerReached()
    {
        animator.SetBool("PlayerReached", true);
        isPlayerReached = true;
        MyGameManager.instance.DisablePlayerFollowCamera();
    }

    public void EnemyDead()
    {
        animator.SetBool("isDead", true);
    }

    public void EnemyVictory()
    {
        animator.SetBool("Victory", true);

    }

    public void DamageTaken()
    {
        health--;
        var tempp = Instantiate(Armor.instance.nonArmorTaken, this.gameObject.transform.position, Quaternion.identity);
        Destroy(tempp, 1f);
        if (health <= 0)
        {
            Enemy.instance.EnemyDead();
            MyGameManager.instance.PlayerVictory();
        }
    }

    public void ReducePlayerArmor()
    {
        Armor.instance.GotHit();
    }
}
