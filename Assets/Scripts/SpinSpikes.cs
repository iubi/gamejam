using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinSpikes : MonoBehaviour
{
    float temp,sensitivity;
    // Start is called before the first frame update
    void Start()
    {
        sensitivity = Random.Range(.5f,1f);
    }

    // Update is called once per frame
    void Update()
    {
        if (MyGameManager.instance.gameStarted)
        {
            temp += (transform.rotation.x + Time.deltaTime )* sensitivity;
            transform.Rotate(new Vector3(temp, transform.rotation.y, transform.rotation.z), Space.Self);
        }
    }
}
