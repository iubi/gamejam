using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MovePlayer : MonoBehaviour
{
    public bool waitInterval = false;
    public float startPos, endPos, sensitivity;

    // Start is called before the first frame update
    void Start()
    {
        DOTween.Init();

    }

    // Update is called once per frame
    void Update()
    {
        // if (Input.GetMouseButtonDown(0)) MoveLeft();
        // else if (Input.GetMouseButtonDown(1)) MoveRight();

        PlayerMovement();

    }

    private void PlayerMovement()
    {

        if (MyGameManager.instance.gameStarted)
        {
            float pointerX = Input.GetAxis("Mouse X");

            if (Input.touchCount > 0)
            {
                pointerX = Input.touches[0].deltaPosition.x * sensitivity;
                gameObject.transform.position = new Vector3(Mathf.Clamp(gameObject.transform.position.x + pointerX, -2f, 2f), gameObject.transform.position.y, gameObject.transform.position.z);
            }
        }
    }

    private void FixedUpdate()
    {
        // SimpleMovement();
    }

    public void MoveLeft()
    {
        if (!waitInterval)
        {
            transform.DOMoveX(Mathf.Clamp(transform.position.x - 2f, -2f, 2f), 1f, false);
            StartCoroutine(WaitingTime());
            waitInterval = true;

        }
    }

    public void MoveRight()
    {
        if (!waitInterval)
        {
            transform.DOMoveX(Mathf.Clamp(transform.position.x + 2f, -2f, 2f), 1f, false);
            StartCoroutine(WaitingTime());
            waitInterval = true;
        }

    }

    IEnumerator WaitingTime()
    {
        yield return new WaitForSeconds(1f);
        waitInterval = false;

    }



    private void SimpleMovement()
    {
        if (MyGameManager.instance.gameStarted)
        {
            // TouchSwipeDirection(fingerIndex);

            if (Input.touchCount > 0)
            {
                var input = Input.GetTouch(0);

                if (input.phase == TouchPhase.Began)
                {
                    startPos = input.position.x * Time.deltaTime * sensitivity;
                }

                else if (input.phase == TouchPhase.Moved)
                {
                    endPos = (input.position.x * Time.deltaTime * sensitivity) - startPos;
                    var myPos = Mathf.Clamp((endPos), -2f, 2f);
                    gameObject.transform.position = new Vector3(Mathf.Clamp(gameObject.transform.position.x + endPos, -2f, 2f), gameObject.transform.position.y, gameObject.transform.position.z);

                }

                else if (input.phase == TouchPhase.Stationary)
                {
                    startPos = input.position.x * Time.deltaTime * sensitivity;
                }

            }
        }
    }
}
