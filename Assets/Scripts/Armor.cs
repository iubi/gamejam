using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Armor : MonoBehaviour
{
    public static Armor instance;
    public GameObject[] armor;
    public Animator animator;
    public int i = 0;
    public float weight = 0;

    public Image armoroImageFiller;
    public GameObject armorTaken, nonArmorTaken;


    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Enemy.instance.isPlayerReached) transform.LookAt(Enemy.instance.transform);
    }


    /// <summary>
    /// OnTriggerEnter is called when the Collider other enters the trigger.
    /// </summary>
    /// <param name="other">The other Collider involved in this collis0ion.</param>
    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.CompareTag("Weapon"))
        {

            MyGameManager.instance.ArmorUp();


            if (i < 2 && i >= 0)
            {
                weight++;
                animator.SetLayerWeight(1, weight / 1f);
            }
            if (i < armor.Length && i >= 0)
            {
                armor[i].SetActive(true);
                i++;
            }
            var temp = Instantiate(armorTaken, collider.gameObject.transform.position, Quaternion.identity);
            Destroy(temp, 1f);
            Destroy(collider.gameObject);

        }
        else if (collider.gameObject.CompareTag("NonWeapon"))
        {
            animator.SetBool("Stumble", true);
            MyGameManager.instance.ArmorDown();
            if (i < 2 && i > 0)
            {
                weight--;
                animator.SetLayerWeight(1, weight / 1);
            }
            if (i > 0)
            {
                i--;
                armor[i].SetActive(false);
            }
            var tempp = Instantiate(nonArmorTaken, collider.gameObject.transform.position, Quaternion.identity);
            Destroy(tempp, 1f);

            Destroy(collider.gameObject, 1.5f);
        }
        else if (collider.gameObject.CompareTag("Finish"))
        {
            // MyGameManager.instance.playerAnimator.SetBool("Victory", true);
            MyGameManager.instance.playerAnimator.runtimeAnimatorController = MyGameManager.instance.playerCombatAnimator;
            if (i >= 1) animator.SetBool("SwordFight", true);
            else animator.SetBool("SwordFight", false);


            Enemy.instance.PlayerReached();
            MyGameManager.instance.AssignPlayerToCineMachineCameraLookAt();
            MyGameManager.instance.attackBTN.SetActive(true);

            // StartCoroutine(CallEndingAnimations());
        }
        armoroImageFiller.fillAmount = (float)i / (float)armor.Length;


    }


    IEnumerator CallEndingAnimations()
    {
        yield return new WaitForSeconds(5f);
        if ((i * 100) / armor.Length > MyGameManager.instance.winningQuota)
        {
            Enemy.instance.EnemyDead();
            MyGameManager.instance.PlayerVictory();
        }
        else
        {
            Enemy.instance.EnemyVictory();
            MyGameManager.instance.PlayerDead();
        }
        MyGameManager.instance.gameStarted = false;
    }


    public void DisableStumblingBool()
    {
        animator.SetBool("Stumble", false);
    }



    public void GotHit()
    {
        if (i > 0)
        {
            MyGameManager.instance.ArmorDown();
           
            if (i > 0)
            {
                i--;
                armor[i].SetActive(false);
                armoroImageFiller.fillAmount = (float)i / (float)armor.Length;
            }
            if (i == 0)
            {
                animator.SetTrigger("Kick");
            }
            var tempp = Instantiate(nonArmorTaken, this.gameObject.transform.position, Quaternion.identity);
            Destroy(tempp, 1f);


        }
        else
        {
            Enemy.instance.EnemyVictory();
            MyGameManager.instance.PlayerDead();
        }
    }


    public void ReducePlayerArmor()
    {
        Enemy.instance.DamageTaken();
    }
}
