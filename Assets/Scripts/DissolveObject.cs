﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Renderer))]
public class DissolveObject : MonoBehaviour
{
    [SerializeField] private float noiseStrength = 0.25f;
    [SerializeField] private float objectHeight = 1.0f;
    public float threshold = 0;

    private Material material;

    void OnEnable()
    {
        StartCoroutine(StartFilling());
    }

    void OnDisable()
    {
        threshold = 1;
    }
    private void Awake()
    {
        material = GetComponent<Renderer>().material;
    }

    private void Update()
    {
        // var time = Time.time * Mathf.PI * 0.25f;

        // float height = 0;
        // height += 0.00001f;
        // SetHeight(height);
    }

    private void SetHeight(float height)
    {
        material.SetFloat("_CutoffHeight", height);
        material.SetFloat("_NoiseStrength", noiseStrength);
    }

    IEnumerator StartFilling()
    {
        yield return new WaitForSeconds(.05f);
        threshold += 0.1f;
        SetHeight(threshold);
        if (threshold < 10)
        {
            StartCoroutine(StartFilling());
        }
    }
}
