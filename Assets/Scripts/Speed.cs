using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Speed : MonoBehaviour
{
    public Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();

        animator.SetFloat("Speed", Random.Range(0.1f, .6f));
    }

    // Update is called once per frame
    void Update()
    {

    }
}
