using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialEnabler : MonoBehaviour
{
    public Material testFiller;
    public MeshRenderer testFillerMR;
    public float threshold = 1;



    void OnEnable()
    {
        StartCoroutine(StartFilling());
    }




    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator StartFilling()
    {
        yield return new WaitForSeconds(.05f);
        threshold -= 0.01f;
        testFiller.SetFloat("Threshold", threshold);
        testFillerMR.sharedMaterial.SetFloat("Thhh", threshold);
        if (threshold > 0)
        {
            StartCoroutine(StartFilling());
        }
    }
}
